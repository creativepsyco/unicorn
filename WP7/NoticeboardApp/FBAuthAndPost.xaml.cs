﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Facebook;

namespace NoticeboardApp
{
    public partial class FBAuthAndPost : PhoneApplicationPage
    {
        private const string _appId = "149685561770321";
        private const string _appSecret = "05f18af268e822e58f581944f821ff62"; //Secret Do not share ;)
        private readonly string[] _extendedPermissions = new[] { "user_about_me read_stream publish_stream" };

        private bool _loggedIn = false;

        private FacebookClient _fbClient;

        // At this point we have an access token so we can get information from facebook

        private void MakeANoticeBoardPost(string message,string description,
                                          string link, string picture,
                                          string caption, string name)
        {
            var parameters = new Dictionary<string, object>
                             {
                                 {"message",message},
                                 {"link",link},
                                 {"picture",picture},
                                 {"name",name},
                                 {"caption",caption},
                                 {"description",description},
                                 {"privacy",new Dictionary<string,object>
                                    {
                                        {"value","ALL_FRIENDS"}
                                    }
                                 }
                             };
            _fbClient.PostCompleted += _fbClient_PostCompleted;
            _fbClient.PostAsync("me/feed",parameters);
        }

        void _fbClient_PostCompleted(object sender, FacebookApiEventArgs e)
        {
            //Handle here watever u wanna do after posting
            if (e.Error == null)
            {
                var result = (IDictionary<string, object>)e.GetResultData();
                Dispatcher.BeginInvoke(() => MyData.ItemsSource = result);
            }
            else
            {
                MessageBox.Show(e.Error.Message);
            }
        }

        private void loginSucceeded()
        {
            TitlePanel.Visibility = Visibility.Visible;
            FacebookLoginBrowser.Visibility = Visibility.Collapsed;
            InfoPanel.Visibility = Visibility.Visible;
            
            // Make a Post this way
            MakeANoticeBoardPost("This is a trial post", "Lorem Ipsum Lorem Ipsum Lorem Ipsum",
                "http://www.google.com", "http://code.google.com/images/GSoC2011_300x200.png",
                "Search Engines FTW!!", "Another Testing Post");
        }

        // Constructor
        public FBAuthAndPost()
        {
            InitializeComponent();
            _fbClient = new FacebookClient(_appId, _appSecret);
            FacebookLoginBrowser.Loaded += new RoutedEventHandler(FacebookLoginBrowser_Loaded);
        }

        // Browser control is loaded and fully ready for use
        void FacebookLoginBrowser_Loaded(object sender, RoutedEventArgs e)
        {
            if (!_loggedIn)
            {
                LoginToFacebook();
            }
        }

        // This handles the display a little and also creates the initial URL to navigate to in the browser control
        private void LoginToFacebook()
        {
            TitlePanel.Visibility = Visibility.Visible;
            FacebookLoginBrowser.Visibility = Visibility.Visible;
            InfoPanel.Visibility = Visibility.Collapsed;

            var loginParameters = new Dictionary<string, object>
                                      {
                                          { "response_type", "token" }
                                          // { "display", "touch" } // by default for wp7 builds only (in Facebook.dll), display is set to touch.
                                      };

            var navigateUrl = FacebookOAuthClient.GetLoginUrl(_appId, null, _extendedPermissions, loginParameters);

            FacebookLoginBrowser.Navigate(navigateUrl);
        }

        private void FacebookLoginBrowser_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            FacebookOAuthResult oauthResult;
            if (FacebookOAuthResult.TryParse(e.Uri, out oauthResult))
            {
                if (oauthResult.IsSuccess)
                {
                    _fbClient = new FacebookClient(oauthResult.AccessToken);
                    _loggedIn = true;
                    loginSucceeded();
                }
                else
                {
                    MessageBox.Show(oauthResult.ErrorDescription);
                }
            }
        }
    }
}