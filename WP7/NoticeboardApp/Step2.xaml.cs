﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace NoticeboardApp
{
    public partial class Step2 : PhoneApplicationPage
    {
        public Step2()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Step2_Loaded);
        }

        void Step2_Loaded(object sender, RoutedEventArgs e)
        {
            if (App.Post.ContainsKey("title"))
            {
                title_tb.Text = App.Post["title"].ToString();
            }
            if (App.Post.ContainsKey("description"))
            {
                description_tb.Text = App.Post["description"].ToString();
            }
        }
        private void PhoneApplicationPage_GotFocus(object sender, RoutedEventArgs e)
        {
            content_pnl.Margin = new Thickness(0, -40, 0, 0);
        }

        private void description_tb_LostFocus(object sender, RoutedEventArgs e)
        {
            content_pnl.Margin = new Thickness(0, 0, 0, 0);
        }

        private void title_tb_GotFocus(object sender, RoutedEventArgs e)
        {
            if (title_tb.Text == "Title")
            {
                title_tb.Text = "";
                SolidColorBrush Brush1 = new SolidColorBrush();
                Brush1.Color = Colors.Black;
                title_tb.Foreground = Brush1;
            }
        }

        private void title_tb_LostFocus(object sender, RoutedEventArgs e)
        {
            if (title_tb.Text == String.Empty)
            {
                title_tb.Text = "Title";
                SolidColorBrush Brush2 = new SolidColorBrush();
                Brush2.Color = Colors.Gray;
                title_tb.Foreground = Brush2;
            }
        }
        private void next_btn_Click(object sender, RoutedEventArgs e)
        {
            App.AddPostArgument("title", title_tb.Text);
            App.AddPostArgument("description", description_tb.Text);
            NavigationService.Navigate(new Uri("/Step3.xaml", UriKind.Relative));
        }
    }
}