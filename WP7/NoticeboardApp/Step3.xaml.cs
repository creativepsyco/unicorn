﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.ServiceModel;

namespace NoticeboardApp
{
    public partial class Step3 : PhoneApplicationPage
    {
        public Step3()
        {
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(Step3_Loaded);
         }

        protected override void  OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            price_lbl.Text = "Navigate2";
 	        base.OnNavigatedTo(e);
        }
        void Step3_Loaded(object sender, RoutedEventArgs e)
        {
            title_lbl.Text = App.Post["title"].ToString();
            category_lbl.Text = App.Post["category"].ToString();
            date_lbl.Text = DateTime.Now.ToLongDateString();
            price_lbl.Text = "Loaded";
            description_lbl.Text = App.Post["description"].ToString();
        }

        private void post_btn_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}