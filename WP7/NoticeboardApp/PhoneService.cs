﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace NoticeboardApp
{
    public class PhoneService
    {
        private static PhoneService phoneServiceInstance = null;
        public CameraService cameraServiceInstance { get; private set; }
        private PhoneService()
        {
            cameraServiceInstance = new CameraService();
        }

        public static PhoneService GetInstance()
        {
            if (phoneServiceInstance == null)
                phoneServiceInstance = new PhoneService();
            return phoneServiceInstance;
        }
    }
}
