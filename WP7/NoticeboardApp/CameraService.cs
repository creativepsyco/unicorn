﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;

namespace NoticeboardApp
{
    public class CameraService
    {
        CameraCaptureTask cameraCaptureTask;
        PhotoChooserTask photoChooserTask;
        BitmapImage bmp;
        bool isCapturing = false;
        bool isPicking = false;
        bool bLastError = false;
        string strLastError = "";

        public CameraService()
        {
            cameraCaptureTask = new CameraCaptureTask();
            cameraCaptureTask.Completed += new EventHandler<PhotoResult>(photoCaptureOrSelectionCompleted);
            photoChooserTask = new PhotoChooserTask();
            photoChooserTask.Completed += new EventHandler<PhotoResult>(photoCaptureOrSelectionCompleted);
        }

        public BitmapImage GetLastImage()
        {
            return bmp;
        }

        public void StartCaptureTask()
        {
            cameraCaptureTask.Show();
            isCapturing = true;
        }

        public void StartPhotoChooserTask()
        {
            isPicking = true;
            photoChooserTask.Show();
        }

        void photoCaptureOrSelectionCompleted(object sender, PhotoResult e)
        {
            bLastError = false;
            if (e.TaskResult == TaskResult.OK)
            {
                bmp = new BitmapImage();
                bmp.SetSource(e.ChosenPhoto);
            }
            else
            {
                bLastError = true;
                strLastError = "Task Result Error: " + e.TaskResult.ToString();
            }
        }

        void cameraCaptureTask_Completed(object sender, PhotoResult e)
        {
            throw new NotImplementedException();
        }

    }
}
