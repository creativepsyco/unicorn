﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace NoticeboardApp
{
    public partial class Step1 : PhoneApplicationPage
    {
        public Step1()
        {
            InitializeComponent();
            this.Loaded += new RoutedEventHandler(Step1_Loaded);
        }
        protected override void  OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (App.Post.ContainsKey("type"))
            {
                DoCheck(App.Post["type"].ToString());
            }
            if (App.Post.ContainsKey("category"))
            {
                category_lp.SelectedItem = App.Post["category"].ToString();
            }

 	        base.OnNavigatedTo(e);
        }
        void Step1_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void DoCheck(string type)
        {
            if (type == "Notice")
            {
                notice_rd.IsChecked = true;
            }
            else
            {
                request_rd.IsChecked = true;
            }
        }

        private void next_btn_Click(object sender, RoutedEventArgs e)
        {
            string type = "";
            if(notice_rd.IsChecked == true)
                type = "Notice";
            else
                type = "Request";

            App.AddPostArgument("type", type);
            App.AddPostArgument("category", category_lp.SelectedItem.ToString());
            NavigationService.Navigate(new Uri("/Step2.xaml", UriKind.Relative));
        }
    }
}