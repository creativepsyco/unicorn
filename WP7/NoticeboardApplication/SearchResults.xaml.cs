﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using NoticeboardApplication.ServiceReference1;

namespace NoticeboardApplication
{
    public partial class SearchResults : PhoneApplicationPage
    {
        List<Notice> noticeResult;
        List<Request> requestResult;
        public SearchResults()
        {
            InitializeComponent();
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            allNotice.SelectedItem = null;
            allRequest.SelectedItem = null;
            allNotice.Visibility = System.Windows.Visibility.Visible;
            allRequest.Visibility = System.Windows.Visibility.Collapsed;
            base.OnNavigatedTo(e);
        }

        private void search_tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SearchBoxDisappear.Begin();
                NoticeboardServiceClient client = new NoticeboardServiceClient();
                client.SearchNoticesCompleted += new EventHandler<SearchNoticesCompletedEventArgs>(client_SearchNoticesCompleted);
                client.SearchNoticesAsync(search_tb.Text);
                client.SearchRequestsCompleted += new EventHandler<SearchRequestsCompletedEventArgs>(client_SearchRequestsCompleted);
                client.SearchRequestsAsync(search_tb.Text);
                filter.Visibility = System.Windows.Visibility.Visible;
                progressbar.IsIndeterminate = true;
            }
        }

        void client_SearchRequestsCompleted(object sender, SearchRequestsCompletedEventArgs e)
        {
            requestResult = e.Result.ToList<Request>();
            allRequest.ItemsSource = requestResult;
        }

        void client_SearchNoticesCompleted(object sender, SearchNoticesCompletedEventArgs e)
        {
            noticeResult = e.Result.ToList<Notice>();
            allNotice.ItemsSource = noticeResult;
            progressbar.IsIndeterminate = false;

        }

        private void allNotice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (allNotice.SelectedItem != null)
            {
                Notice n = (Notice)allNotice.SelectedItem;
                navigate(n);
            }
        }
        private void allRequest_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (allRequest.SelectedItem != null)
            {
                Request r = (Request)allRequest.SelectedItem;
                navigate(r);
            }
        }

        private void jobfilter_lp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (jobfilter_lp.SelectedIndex == 0)
            {
                allNotice.Visibility = System.Windows.Visibility.Visible;
                allRequest.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (jobfilter_lp.SelectedIndex == 1)
            {
                allRequest.Visibility = System.Windows.Visibility.Visible;
                allNotice.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void navigate(Request n)
        {
            App.ViewPost.Clear();
            App.ViewPost.Add("id", n.id);
            App.ViewPost.Add("title", n.title);
            App.ViewPost.Add("description", n.description);
            App.ViewPost.Add("category", n.category);
            App.ViewPost.Add("type", "Request");
            App.ViewPost.Add("timestamp", n.timestamp.ToString());
            App.ViewPost.Add("userid", n.userid);
            App.ViewPost.Add("photourl", n.photourl);
            App.ViewPost.Add("handphone", n.phone);
            NavigationService.Navigate(new Uri("/ViewPost.xaml", UriKind.Relative));
        }
        private void navigate(Notice n)
        {
            App.ViewPost.Clear();
            App.ViewPost.Add("id", n.id);
            App.ViewPost.Add("title", n.title);
            App.ViewPost.Add("description", n.description);
            App.ViewPost.Add("price", n.price);
            App.ViewPost.Add("category", n.category);
            App.ViewPost.Add("type", "Notice");
            App.ViewPost.Add("timestamp", n.timestamp.ToString());
            App.ViewPost.Add("userid", n.userid);
            App.ViewPost.Add("handphone", n.phone);
            App.ViewPost.Add("photourl", n.photourl);
            NavigationService.Navigate(new Uri("/ViewPost.xaml", UriKind.Relative));
        }
        private void Search_pnl_Loaded(object sender, RoutedEventArgs e)
        {
            SearchBoxAnimation.Begin();
            search_tb.Focus();
        }
    }
}