﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace NoticeboardApplication
{
    public class URITemplate
    {
        /// <summary>
        /// {0} => APIKey
        /// {1} => Token, put in blank if no token
        /// </summary>
        public static readonly string LAPI_Validate = "https://ivle.nus.edu.sg/api/Lapi.svc/Validate?APIKey={0}&Token={1}";
        /// <summary>
        /// {0} => APIKey
        /// {1} => Token
        /// {2} => Boolean for retrieving Title Only
        /// </summary>
        public static readonly string LAPI_Student_Events = "http://ivle.nus.edu.sg/api/Lapi.svc/StudentEvents?APIKey={0}&AuthToken={1}&TitleOnly={2}";

        /// <summary>
        /// Lapi Data Call URI Template Demo Data
        /// </summary>
        public static readonly string LAPI_Student_Particulars = "http://mobapps.nus.edu.sg/api/student/A0002345B/";
        /// <summary>
        /// {0} => API Key
        /// {1} => Auth Token
        /// </summary>
        public static readonly string LAPI_Student_User_Name = "https://ivle.nus.edu.sg/api/Lapi.svc/UserName_Get?APIKey={0}&Token={1}";
    }
}
