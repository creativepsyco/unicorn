﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Facebook;

namespace NoticeboardApplication
{
    public class FBPost
    {
        public string message;
        public string description;
        public string link;
        public string picture;
        public string caption;
        public string name;
    }

    public partial class FBAuthAndPost : PhoneApplicationPage
    {
        private const string _appId = "149685561770321";
        private const string _appSecret = "05f18af268e822e58f581944f821ff62"; //Secret Do not share ;)
        private readonly string[] _extendedPermissions = new[] { "user_about_me read_stream publish_stream" };

        private bool _loggedIn = false;

        private FacebookClient _fbClient;
        private FBPost param = null;
        public static bool callPost = false;
        public static returnBooleanCallback callback = null;
        public static void MakeAPost(string message, string description,
                                          string link, string picture,
                                          string caption, string name, returnBooleanCallback calledback)
        {
            FBAuthAndPost obj = new FBAuthAndPost();
            obj.MakeANoticeBoardPost(message, description, link, picture, caption, name, calledback);
        }
        // At this point we have an access token so we can get information from facebook


        private void MakeANoticeBoardPost(FBPost p, returnBooleanCallback calledback)
        {
            MakeANoticeBoardPost(p.message, p.description, p.link, p.picture, p.caption, p.name, calledback);
        }

        private void MakeANoticeBoardPost(string message, string description,
                                          string link, string picture,
                                          string caption, string name,
                                          returnBooleanCallback calledback)
        {
            param = null;
            callPost = false;
            var parameters = new Dictionary<string, object>
                             {
                                 {"message",message},
                                 {"link",link},
                                 {"picture",picture},
                                 {"name",name},
                                 {"caption",caption},
                                 {"description",description},
                                 {"privacy",new Dictionary<string,object>
                                    {
                                        {"value","ALL_FRIENDS"}
                                    }
                                 }
                             };
            FBAuthAndPost.callback = calledback;
            try
            {
                _fbClient.PostCompleted += _fbClient_PostCompleted;
                _fbClient.PostAsync("me/feed", parameters);
            }
            catch (Exception e) { if (FBAuthAndPost.callback != null) { FBAuthAndPost.callback(false); OfflineStorageClass.FacebookToken = ""; } }

        }

        void _fbClient_PostCompleted(object sender, FacebookApiEventArgs e)
        {
            //Handle here watever u wanna do after posting
            if (e.Error == null)
            {
                if (FBAuthAndPost.callback != null)
                    FBAuthAndPost.callback(true);
                var result = (IDictionary<string, object>)e.GetResultData();
                //Dispatcher.BeginInvoke(() => MyData.ItemsSource = result);
            }
            else
            {
                if (FBAuthAndPost.callback != null)
                {
                    FBAuthAndPost.callback(false);
                    OfflineStorageClass.FacebookToken = "";
                }
                ///TODO: Need to be handled later
                //MessageBox.Show(e.Error.Message);
            }
        }

        private void loginSucceeded()
        {
            //store the token
            OfflineStorageClass.FacebookToken = _fbClient.AccessToken;
            TitlePanel.Visibility = Visibility.Visible;
            FacebookLoginBrowser.Visibility = Visibility.Collapsed;
            InfoPanel.Visibility = Visibility.Visible;
            if (callPost)
            {
                MakeANoticeBoardPost("I am using the NUS Noticeboard to sell/buy/share my stuff.", App.Post["description"].ToString(), "http://google.com", "https://lh3.googleusercontent.com/-pFwcmnUkSP8/TiLlXDMU-pI/AAAAAAAAAyM/89Z2ugd4lZo/nb.png",
                        App.StudentInfo["name"] + " is using Noticeboard", App.Post["title"].ToString(), FBAuthAndPost.callback);
                //App.Post.Clear();
            }
            else
            {
                _fbClient.GetCompleted += new EventHandler<FacebookApiEventArgs>(_fbClient_GetCompleted);
                _fbClient.GetAsync("me");
            }
            Dispatcher.BeginInvoke(() => { MessageBox.Show("Successfully connected Facebook account"); this.NavigationService.GoBack(); });
        }

        void _fbClient_GetCompleted(object sender, FacebookApiEventArgs e)
        {
            var result = (IDictionary<string, object>)e.GetResultData();
            // Make a Post this way
            MakeANoticeBoardPost(result["first_name"] + " has connected his accounts with NoticeBoard.", "Noticeboard is a windows phone 7 application which allows students from the National University of Singapore to buy, sell, borrow or share their stuff with the rest of the student population.",
                "http://www.google.com", "https://lh3.googleusercontent.com/-pFwcmnUkSP8/TiLlXDMU-pI/AAAAAAAAAyM/89Z2ugd4lZo/nb.png",
                "A Sharing/Caring venture for fellow NUS students brought to you by AppSycos", "Noticeboard Application for Windows Phone 7", null);
        }

        // Constructor
        public FBAuthAndPost()
        {
            InitializeComponent();
            webBrowser1.Visibility = System.Windows.Visibility.Collapsed;
            _fbClient = new FacebookClient(_appId, _appSecret);
            if (OfflineStorageClass.FacebookToken == "")
            {
                FacebookLoginBrowser.Loaded += new RoutedEventHandler(FacebookLoginBrowser_Loaded);
                LoggedInStatus.Visibility = System.Windows.Visibility.Collapsed;
                grid1.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                _fbClient.AccessToken = OfflineStorageClass.FacebookToken;
                FacebookLoginBrowser.Visibility = Visibility.Collapsed;
                InfoPanel.Visibility = Visibility.Collapsed;
                LoggedInStatus.Visibility = System.Windows.Visibility.Visible;
                grid1.Visibility = System.Windows.Visibility.Visible;
                //loginSucceeded();
            }
        }

        // Browser control is loaded and fully ready for use
        void FacebookLoginBrowser_Loaded(object sender, RoutedEventArgs e)
        {
            if (!_loggedIn)
            {
                LoginToFacebook();
            }
        }

        // This handles the display a little and also creates the initial URL to navigate to in the browser control
        private void LoginToFacebook()
        {
            TitlePanel.Visibility = Visibility.Visible;
            FacebookLoginBrowser.Visibility = Visibility.Visible;
            InfoPanel.Visibility = Visibility.Collapsed;

            var loginParameters = new Dictionary<string, object>
                                      {
                                          { "response_type", "token" }
                                          // { "display", "touch" } // by default for wp7 builds only (in Facebook.dll), display is set to touch.
                                      };

            var navigateUrl = FacebookOAuthClient.GetLoginUrl(_appId, null, _extendedPermissions, loginParameters);

            FacebookLoginBrowser.Navigate(navigateUrl);
        }

        private void FacebookLoginBrowser_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            FacebookOAuthResult oauthResult;
            if (FacebookOAuthResult.TryParse(e.Uri, out oauthResult))
            {
                if (oauthResult.IsSuccess)
                {
                    _fbClient = new FacebookClient(oauthResult.AccessToken);
                    _loggedIn = true;
                    loginSucceeded();
                }
                else
                {
                    MessageBox.Show(oauthResult.ErrorDescription);
                }
            }
        }

        private void button1_click(object sender, EventArgs e)
        {
            WebClient w = new WebClient();
            w.DownloadStringCompleted += new DownloadStringCompletedEventHandler(w_DownloadStringCompleted);
            w.DownloadStringAsync(new Uri("https://www.facebook.com/logout.php?next=http://google.com&access_token=" +OfflineStorageClass.FacebookToken));
        }

        void web_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {
            OfflineStorageClass.FacebookToken = "";
            Dispatcher.BeginInvoke(() => { MessageBox.Show("Successfully Logged Out"); this.NavigationService.GoBack(); });
        }

        void w_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            OfflineStorageClass.FacebookToken = "";
            Dispatcher.BeginInvoke(() => { MessageBox.Show("Successfully Logged Out"); this.NavigationService.GoBack(); });
        }

        private void button2_click(object sender, EventArgs e)
        {
            loginSucceeded();
        }
        private void logo_click(object sender, MouseButtonEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }
    }
}