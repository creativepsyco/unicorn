﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;
using System.IO;
using System.IO.IsolatedStorage;

namespace NoticeboardApplication
{
    public partial class MainPage3 : PhoneApplicationPage
    {
        public MainPage3()
        {
            InitializeComponent();
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            menu.SelectedIndex = -1;
            base.OnNavigatedTo(e);
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (menu.SelectedIndex == 0)
            {
                NavigationService.Navigate(new Uri("/Step1.xaml", UriKind.Relative));
            }
            if (menu.SelectedIndex == 1)
            {
                NavigationService.Navigate(new Uri("/Home.xaml", UriKind.Relative));
            }
            if (menu.SelectedIndex == 2)
            {
                NavigationService.Navigate(new Uri("/SearchResults.xaml", UriKind.Relative));
            }
        }
    }
}