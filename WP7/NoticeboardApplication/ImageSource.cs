﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Globalization;
using System.Windows.Media.Imaging;
using System.IO;

namespace NoticeboardApplication
{
    public class ImageFormat : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                BitmapImage bitmapImage = new BitmapImage();
                MemoryStream stream = new MemoryStream(((NoticeboardApplication.ServiceReference1.PictureFile)value).PictureStream);
                bitmapImage.SetSource(stream);
                return bitmapImage;
            }
            return value;
        }
        public object ConvertBack(object value, Type targetType,
                                  object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                return DateTime.Parse(value.ToString());
            }
            return value;
        }
    }

}
