﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using NoticeboardApplication.ServiceReference1;

namespace NoticeboardApplication
{
    public partial class MainPage : PhoneApplicationPage
    {
        NoticeboardServiceClient myStoreClient;
        // res;

        int countBook = 0;
        static bool mystoreInitialized = false;
        public MainPage()
        {
            InitializeComponent();
            prog.IsIndeterminate = true;
            progbar.IsIndeterminate = true;
            myStoreClient = new NoticeboardServiceClient();
            myStoreClient.GetNoticesByUserCompleted += new EventHandler<GetNoticesByUserCompletedEventArgs>(myStoreClient_GetNoticesByUserCompleted);
            myStoreClient.GetNoticesByUserAsync(App.StudentInfo["userid"].ToString());
            myStoreClient.GetRequestsByUserCompleted += new EventHandler<GetRequestsByUserCompletedEventArgs>(myStoreClient_GetRequestsByUserCompleted);
            myStoreClient.GetRequestsByUserAsync(App.StudentInfo["userid"].ToString());
            myStoreClient.GetNewNoticesCompleted += new EventHandler<GetNewNoticesCompletedEventArgs>(myStoreClient_GetNewNoticesCompleted);
            myStoreClient.GetNewNoticesAsync();
            myStoreClient.GetNewRequestsCompleted += new EventHandler<GetNewRequestsCompletedEventArgs>(myStoreClient_GetNewRequestsCompleted);
            myStoreClient.GetNewRequestsAsync();
            LAPICaller.RetrieveUserName(cLAPI.API_Token, StoreUserName);
            if (mystoreInitialized)
            {
                txtUserName.Text = App.StudentInfo["name"];
                txtUserEmail.Text = App.StudentInfo["email"];
                txtUserPhone.Text = App.StudentInfo["handphone"];
            }
            else
            {
                txtUserName.Text = "loading...";
                txtUserEmail.Text = "loading...";
                txtUserPhone.Text = "loading...";
            }
            //res = new List<Notice>();
        }

        private void StoreUserName(string s)
        {
            PhoneUtils.ForceAddToDictionary("name", s.ToUpperInvariant(), App.StudentInfo); 
            LAPIDataCall.GetStudentInfo(getStudentInfoHandler);
        }
        
        void getStudentInfoHandler(Dictionary<string, string> info)
        {
            
            Dispatcher.BeginInvoke(() => {
                txtUserName.Text = App.StudentInfo["name"];
                txtUserEmail.Text = App.StudentInfo["email"];
                txtUserPhone.Text = App.StudentInfo["handphone"];
            });
            mystoreInitialized = true;
        }

        void myStoreClient_GetNewRequestsCompleted(object sender, GetNewRequestsCompletedEventArgs e)
        {
            ClearAndAddRequestToListbox(e.Result.ToList<Request>(), newRequest);
        }

        void myStoreClient_GetNewNoticesCompleted(object sender, GetNewNoticesCompletedEventArgs e)
        {
            progbar.IsIndeterminate = false;
            ClearAndAddNoticeToListbox(e.Result.ToList<Notice>(), newNotice);
        }

        void myStoreClient_GetRequestsByUserCompleted(object sender, GetRequestsByUserCompletedEventArgs e)
        {
            List<Request> res = e.Result.ToList<Request>();
            ClearAndAddRequestToListbox(res, myRequest);
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            LAPICaller.RetrieveUserName(cLAPI.API_Token, StoreUserName);
            if (mystoreInitialized)
            {
                txtUserName.Text = App.StudentInfo["name"];
                txtUserEmail.Text = App.StudentInfo["email"];
                txtUserPhone.Text = App.StudentInfo["handphone"];

            }
            else
            {
                txtUserName.Text = "loading...";
                txtUserEmail.Text = "loading...";
                txtUserPhone.Text = "loading...";
            }
            menu.SelectedIndex = -1;
            myNotice.SelectedItem = null;
            myRequest.SelectedItem = null;
            myNotice.Visibility = System.Windows.Visibility.Visible;
            myRequest.Visibility = System.Windows.Visibility.Collapsed;
            newNotice.Visibility = System.Windows.Visibility.Visible;
            newRequest.Visibility = System.Windows.Visibility.Collapsed;
            newfilter_lp.SelectedIndex = 0;
            jobfilter_lp.SelectedIndex = 0;
            App.fromMainPage = true;
            base.OnNavigatedTo(e);
        }

        void myStoreClient_GetNoticesByUserCompleted(object sender, GetNoticesByUserCompletedEventArgs e)
        {
            List<Notice> res = e.Result.ToList<Notice>();
            ClearAndAddNoticeToListbox(res, myNotice);
            prog.IsIndeterminate = false;
        }

        private void ClearAndAddRequestToListbox(List<Request> list, ListBox listbox)
        {
            listbox.Items.Clear();
            foreach (Request n in list)
            {
                listbox.Items.Add(n);
            }

        }
        private void ClearAndAddNoticeToListbox(List<Notice> list, ListBox listbox)
        {
            listbox.Items.Clear();
            foreach (Notice n in list)
            {
                listbox.Items.Add(n);
            }

        }


        private void myNotice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (myNotice.SelectedItem != null)
            {
                if (myNotice.SelectedItem is Notice)
                {
                    Notice n = (Notice)myNotice.SelectedItem;
                    navigate(n);
                }
            }
        }

        private void myRequest_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (myRequest.SelectedItem != null)
            {
                if (myRequest.SelectedItem is Request)
                {
                    Request n = (Request)myRequest.SelectedItem;
                    navigate(n);
                }
            }
        }

        private void newNotice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (newNotice.SelectedItem != null)
            {
                if (newNotice.SelectedItem is Notice)
                {
                    Notice n = (Notice)newNotice.SelectedItem;
                    navigate(n);
                }
            }
        }

        private void newRequest_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (newRequest.SelectedItem != null)
            {
                if (newRequest.SelectedItem is Request)
                {
                    Request n = (Request)newRequest.SelectedItem;
                    navigate(n);
                }
            }
        }

        //post items in the my Store... Code copied from Home/Books
        private void navigate(Notice n)
        {
            App.ViewPost.Clear();
            App.ViewPost.Add("title", n.title);
            App.ViewPost.Add("description", n.description);
            App.ViewPost.Add("price", n.price);
            App.ViewPost.Add("category", n.category);
            App.ViewPost.Add("timestamp", n.timestamp.ToString());
            App.ViewPost.Add("photourl", n.photourl);
            App.ViewPost.Add("userid", n.userid);
            App.ViewPost.Add("type", "Notice");
            App.ViewPost.Add("id", n.id);
            App.ViewPost.Add("handphone", n.phone);
            NavigationService.Navigate(new Uri("/ViewPost.xaml", UriKind.Relative));
        }

        private void navigate(Request n)
        {
            App.ViewPost.Clear();
            App.ViewPost.Add("title", n.title);
            App.ViewPost.Add("description", n.description);
            App.ViewPost.Add("category", n.category);
            App.ViewPost.Add("timestamp", n.timestamp.ToString());
            App.ViewPost.Add("photourl", n.photourl);
            App.ViewPost.Add("userid", n.userid);
            App.ViewPost.Add("type", "Request");
            App.ViewPost.Add("id", n.id);
            App.ViewPost.Add("handphone", n.phone);
            NavigationService.Navigate(new Uri("/ViewPost.xaml", UriKind.Relative));
        }
        //-------------------------------------------------------------------------------

        //item 1 of main page-------->
        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            switch (menu.SelectedIndex)
            {
                case 0: NavigationService.Navigate(new Uri("/Step1.xaml", UriKind.Relative)); break;
                case 1: NavigationService.Navigate(new Uri("/Home.xaml", UriKind.Relative)); break;
                case 2: NavigationService.Navigate(new Uri("/SearchResults.xaml", UriKind.Relative)); break;
                case 3: NavigationService.Navigate(new Uri("/Settings.xaml", UriKind.Relative)); break;
            }
        }

        private void jobfilter_lp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (jobfilter_lp.SelectedIndex == 0)
            {
                myNotice.Visibility = System.Windows.Visibility.Visible;
                myRequest.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                myRequest.Visibility = System.Windows.Visibility.Visible;
                myNotice.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
        private void newfilter_lp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (newfilter_lp.SelectedIndex == 0)
            {
                newNotice.Visibility = System.Windows.Visibility.Visible;
                newRequest.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                newRequest.Visibility = System.Windows.Visibility.Visible;
                newNotice.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
    }
}