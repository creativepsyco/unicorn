﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;

namespace NoticeboardApplication
{
    public partial class Step2b : PhoneApplicationPage
    {
        PhotoChooserTask photoChooserTask;
        CameraCaptureTask cameraCaptureTask;
        public Step2b()
        {
            InitializeComponent();
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (!App.Post.ContainsKey("type"))
            {
                NavigationService.GoBack();
                return;
                //NavigationService.Navigate(new Uri("/Step1.xaml", UriKind.Relative));
            }
            if (App.Post.ContainsKey("photourl"))
            {
                if (App.Post["photourl"].ToString() != "")
                {
                    BitmapImage image = new BitmapImage(new Uri(App.Post["photourl"].ToString()));
                    img.Source = image;
                    show_img.Visibility = System.Windows.Visibility.Visible;
                    add_img.Visibility = System.Windows.Visibility.Collapsed;
                }
            }
            if (App.Post.ContainsKey("photo"))
            {
                img.Source = (BitmapImage)(App.Post["photo"]);
                show_img.Visibility = System.Windows.Visibility.Visible;
                add_img.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (App.Post.ContainsKey("handphone"))
            {
                if(App.Post["handphone"].ToString() != "")
                    contact_chk.IsChecked = true;
            }
            else
            {
                App.Post.Add("handphone", "");
            }
            contactNo_tb.Text = App.StudentInfo["handphone"].ToString();
            base.OnNavigatedTo(e);
        }
        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (contact_chk.IsChecked == true)
            {
                App.AddPostArgument("handphone", contactNo_tb.Text);
            }
            else
            {
                App.AddPostArgument("handphone","");
            }
            base.OnNavigatedFrom(e);
        }
        private void contact_chk_Checked(object sender, RoutedEventArgs e)
        {
            ContactPanelAppear.Begin();
        }

        private void contact_chk_Unchecked(object sender, RoutedEventArgs e)
        {
            ContactPanelDisappear.Begin();
        }

        private void Grid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            SelectImageAppear.Begin();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            select_img.Visibility = System.Windows.Visibility.Collapsed;

            cameraCaptureTask = new CameraCaptureTask();
            cameraCaptureTask.Completed += new EventHandler<PhotoResult>(photoCaptureOrSelectionCompleted);
            cameraCaptureTask.Show();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            select_img.Visibility = System.Windows.Visibility.Collapsed;

            photoChooserTask = new PhotoChooserTask();
            photoChooserTask.Completed += new EventHandler<PhotoResult>(photoCaptureOrSelectionCompleted);
            photoChooserTask.Show();
        }

        void photoCaptureOrSelectionCompleted(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                BitmapImage bmp = new BitmapImage();
                bmp.SetSource(e.ChosenPhoto);
                App.AddPostArgument("photo", bmp);
            }
        }

        private void post_btn_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Step3.xaml", UriKind.Relative));
        }
    }
}