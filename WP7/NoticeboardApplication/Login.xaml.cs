﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace NoticeboardApplication
{
    public partial class Login : PhoneApplicationPage
    {
        public static bool alreadyLoggedIn;
        // Constructor
        public Login()
        {
            InitializeComponent();
            alreadyLoggedIn = false;
        }

        private void wb_Login_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {

            //this event is triggered when the web browser control has successfully navigated to a url
            var url = e.Uri.ToString();

            //2. when login is complete, the url will be login_result.ashx?r=0
            if (e.Uri.AbsolutePath == "/api/login/login_result.ashx")
            {
                if (url.IndexOf("&r=0") > 0)
                {
                    //3. When login is successful, there will be a &r=0 in the url. It also means the return data is the token itself.
                    //However as this is a web browser, the reply will be wrapped inside the <body> tag, therefore we use regular expressions to grab the actual token
                    var R = System.Text.RegularExpressions.Regex.Match(wb_Login.SaveToString(), @"body\>(.*?)\<\/body", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    if (R.Success)
                    {
                        //4. Token is now saved as a local variable, you have to figure how to promote this to a global value (e.g isolated storage)
                        var IVLE_Token = R.Groups[1].Value;
                        cLAPI.API_Token = IVLE_Token.ToString();
                        OfflineStorageClass.StoreToken(IVLE_Token.ToString());
                        //5. Now navigate to the main app
                        NavigateToSecondPage();
                    }
                }
                else
                    wb_Login.Navigate(new Uri(cLAPI.LoginURL));
            }
        }

        private void NavigateToSecondPage()
        {
            
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }

        public void StoreUserName(string s)
        {
            PhoneUtils.ForceAddToDictionary("name", s, App.StudentInfo);
        }

        private void CheckIfLogin()
        {
            LAPICaller.ValidateLogin(OfflineStorageClass.GetToken(), NavigationCallbackHandler);
            progbar.IsIndeterminate = true;
        }

        private void NavigationCallbackHandler(bool result)
        {
            if (result)
            {
                LAPICaller.RetrieveUserName(cLAPI.API_Token, StoreUserName);
                progbar.IsIndeterminate = false;
                alreadyLoggedIn = true;
                ///... GO Somewhere else
                TitlePanel.Visibility = System.Windows.Visibility.Collapsed;
                wb_Login.Visibility = System.Windows.Visibility.Collapsed;
                image1.Visibility = System.Windows.Visibility.Visible;
                authtext.Visibility = System.Windows.Visibility.Visible;
                NavigateToSecondPage();
            }
            else
            {
                progbar.IsIndeterminate = false;
                TitlePanel.Visibility = System.Windows.Visibility.Visible;
                wb_Login.Visibility = System.Windows.Visibility.Visible;
                image1.Visibility = System.Windows.Visibility.Collapsed;
                authtext.Visibility = System.Windows.Visibility.Collapsed;
                wb_Login.Navigate(new Uri(cLAPI.LoginURL));
            }
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (alreadyLoggedIn)
            {
                if (App.fromMainPage == true)
                {
                    App.fromMainPage = false;
                    NavigationService.GoBack();
                }
                //button1.Visibility = System.Windows.Visibility.Visible;
                //textBlock1.Visibility = System.Windows.Visibility.Visible;
                ////System.ComponentModel.CancelEventArgs f = new System.ComponentModel.CancelEventArgs(false);
                //PhoneApplicationPage_BackKeyPress(this, f);
            }
            base.OnNavigatedTo(e);
        }

        private void PhoneApplicationPage_Loaded(object sender, RoutedEventArgs e)
        {
            if (!alreadyLoggedIn)
            {
                CheckIfLogin();
            }
            //1. Show the web browser and redirect user to the login page
            //wb_Login.Navigate(new Uri(cLAPI.LoginURL));
        }

        private void PhoneApplicationPage_BackKeyPress(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (alreadyLoggedIn)
            {
                e.Cancel = false;
                this.OnBackKeyPress(e);
            }
            base.OnBackKeyPress(e);
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            CheckIfLogin();
        }
    }
}