﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using NoticeboardApplication.ServiceReference1;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;

namespace NoticeboardApplication
{
    public partial class ViewPost : PhoneApplicationPage
    {
        public ViewPost()
        {
            InitializeComponent();
            ApplicationBar = new ApplicationBar();
            ApplicationBar.IsVisible = true;
            ApplicationBar.IsMenuEnabled = true;
            if (App.ViewPost["userid"].ToString() == App.StudentInfo["userid"].ToString())
            {
                activateMyAppbar();
            }
            else
            {
                activateUserAppbar();
            }
        }
        public void activateMyAppbar()
        {
            ApplicationBarIconButton buttonfb = new ApplicationBarIconButton(new Uri("MS_0000s_0009_facebook1.png", UriKind.Relative));
            buttonfb.Text = "share";
            buttonfb.Click += new EventHandler(buttonfb_Click);
            
            ApplicationBarIconButton button1 = new ApplicationBarIconButton(new Uri("appbar.edit.rest.png", UriKind.Relative));
            button1.Text = "Edit";
            button1.Click += new EventHandler(button1_Click);

            ApplicationBarIconButton button2 = new ApplicationBarIconButton(new Uri("appbar.close.rest.png", UriKind.Relative));
            button2.Text = "Delete";
            button2.Click += new EventHandler(button2_Click);

            ApplicationBar.Buttons.Add(buttonfb);
            ApplicationBar.Buttons.Add(button1);
            ApplicationBar.Buttons.Add(button2);

        }
        public void activateUserAppbar()
        {
            ApplicationBarIconButton button1 = new ApplicationBarIconButton(new Uri("MB_0001_mail1.png", UriKind.Relative));
            button1.Text = "Email";
            button1.Click += new EventHandler(button3_Click);

            ApplicationBarIconButton button2 = new ApplicationBarIconButton(new Uri("MB_0008_phone.png", UriKind.Relative));
            button2.Text = "Call";
            button2.Click += new EventHandler(button4_Click);

            ApplicationBarIconButton button3 = new ApplicationBarIconButton(new Uri("MB_0013_msg2.png", UriKind.Relative));
            button3.Text = "SMS";
            button3.Click += new EventHandler(button5_Click);

            ApplicationBar.Buttons.Add(button1);
            if (App.ViewPost.ContainsKey("handphone"))
            {
                if (App.ViewPost["handphone"].ToString() != "")
                {
                    ApplicationBar.Buttons.Add(button2);
                    ApplicationBar.Buttons.Add(button3);
                }
            }
        }
        void buttonfb_Click(object sender, EventArgs e)
        {
            App.Post = App.ViewPost;
            progressbar.IsIndeterminate = true;
            if (!App.ViewPost.ContainsKey("title"))
            {
                MessageBox.Show("Already Posted to facebook");
                return;
            }
            //A check to see if user is logged in or not
            if (string.IsNullOrEmpty(OfflineStorageClass.FacebookToken))
            {
                FBAuthAndPost.callPost = true;
                FBAuthAndPost.callback = booleanCallbackHandler;
                this.NavigationService.Navigate(new Uri("/FBAuthAndPost.xaml", UriKind.Relative));
            }
            else
            {
                FBAuthAndPost.MakeAPost("I am using the NUS Noticeboard to sell/buy/share my stuff.", App.ViewPost["description"].ToString(), "http://google.com", "https://lh3.googleusercontent.com/-pFwcmnUkSP8/TiLlXDMU-pI/AAAAAAAAAyM/89Z2ugd4lZo/nb.png",
                    App.StudentInfo["name"] + " is using Noticeboard", App.ViewPost["title"].ToString(), booleanCallbackHandler);
                //App.Post.Clear();
            }
        }
        private void booleanCallbackHandler(bool p)
        {
            
            //Do here to disable fb share 
            if (p)
            {
                //App.Post.Clear();
                Dispatcher.BeginInvoke(() => { MessageBox.Show("Successfully Posted to Facebook"); progressbar.IsIndeterminate = false; });
                
            }
            else
            {
                Dispatcher.BeginInvoke(() => { MessageBox.Show("The Posting failed for some reason, Maybe the Access was revoked. Try Again to Authenticate."); progressbar.IsIndeterminate = false; });
                
            }
        }
        void button3_Click(object sender, EventArgs e)
        {
            EmailComposeTask emailComposeTask = new EmailComposeTask();
            emailComposeTask.Subject = "message subject";
            emailComposeTask.Body = "message body";
            emailComposeTask.To = "recipient@example.com";
            emailComposeTask.Show();
        }
        void button4_Click(object sender, EventArgs e)
        {
            PhoneCallTask phoneCallTask = new PhoneCallTask();
            phoneCallTask.PhoneNumber = App.ViewPost["handphone"].ToString();
            phoneCallTask.DisplayName = "Daniel";
            phoneCallTask.Show();
        }
        void button5_Click(object sender, EventArgs e)
        {
            SmsComposeTask smsComposeTask = new SmsComposeTask();
            smsComposeTask.To = App.ViewPost["handphone"].ToString();
            smsComposeTask.Body = "Check out this new app.";
            smsComposeTask.Show();
        }
        void button1_Click(object sender, EventArgs e)
        {
            //App
            App.Post = App.ViewPost;
            App.AddPostArgument("updating", true);
            App.AddPostArgument("updatetype", App.ViewPost["type"]);
            //redirect
            NavigationService.Navigate(new Uri("/Step1.xaml", UriKind.Relative));
        }
        void button2_Click(object sender, EventArgs e)
        {
            NoticeboardServiceClient client = new NoticeboardServiceClient();
            App.Post = App.ViewPost;
            
            if (App.Post["type"].ToString() == "Notice")
            {
                client.DeleteNoticeCompleted += new EventHandler<DeleteNoticeCompletedEventArgs>(client_DeleteNoticeCompleted);
                client.DeleteNoticeAsync((int)App.Post["id"]);
            }
            else
            {
                client.DeleteRequestCompleted += new EventHandler<DeleteRequestCompletedEventArgs>(client_DeleteRequestCompleted);
                client.DeleteRequestAsync((int)App.Post["id"]);
            }
        }

        void client_DeleteRequestCompleted(object sender, DeleteRequestCompletedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }

        void client_DeleteNoticeCompleted(object sender, DeleteNoticeCompletedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            title_lbl.Text = App.ViewPost["title"].ToString();
            category_lbl.Text = App.ViewPost["category"].ToString();
            date_lbl.Text = string.Format("{0:d MMM yy}", App.ViewPost["timestamp"].ToString());
            if (App.ViewPost.ContainsKey("price"))
            {
                if(App.ViewPost["price"] != null)
                    price_lbl.Text = App.ViewPost["price"].ToString();
            }
            description_lbl.Text = App.ViewPost["description"].ToString();
            BitmapImage image = new BitmapImage(new Uri( App.ViewPost["photourl"].ToString()));
            img.Source = image;
            base.OnNavigatedTo(e);
        }
        private void logo_click(object sender, MouseButtonEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }
    }
}