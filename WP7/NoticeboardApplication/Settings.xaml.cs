﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace NoticeboardApplication
{
    public partial class Settings : PhoneApplicationPage
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int selectedIndex = menu.SelectedIndex;
            switch (selectedIndex)
            {
                case 0: //FACEBOOK
                    {
                        NavigationService.Navigate(new Uri("/FBAuthAndPost.xaml", UriKind.Relative));
                    } break; //FACEBOOK
                case 1: //LOGOUT
                    {
                        MessageBoxResult m = MessageBox.Show("Do you really want to quit the App and logout?", "Quit App", MessageBoxButton.OKCancel);
                        if (m == MessageBoxResult.OK)
                        {
                            OfflineStorageClass.StoreToken("");
                            App.GetOut();
                        }
                    }break;
            }
            menu.SelectedIndex = -1;
        }
        private void logo_click(object sender, MouseButtonEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }
    }
}