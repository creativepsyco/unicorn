﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Xml;
using System.IO;

namespace NoticeboardApplication
{
    public class LAPIDataCall
    {
        public static string userid
        {
            get
            {
                return "A0002345B";
            }
        }

        public static string name
        {
            get
            {
                return "Amulya Khare";

            }
        }

        public static string email
        {
            get
            {
                return userid + "@nus.edu.sg";
            }
        }

        public static string handphone
        {
            get
            {
                return "62345678";
            }
        }

        /// <summary>
        /// Returns the following after querying from IVLE Sample Data Call
        /// <partics name="ANONYMOUS STUD 2" handphone="92345678"
        ///  home_address="Blk XXX PUNGGOL CENTRAL, #13-134, SINGAPORE 820xxx, SINGAPORE" 
        ///  home_number="62345678" mail_number="" 
        ///  mailing_address="Blk XXX PUNGGOL CENTRAL, #13-134, SINGAPORE 820xxx, SINGAPORE" 
        ///  matric="A0002345B"/>
        ///  To use the student information, just use the fields above namely:-
        ///  name :
        ///  handphone :
        ///  home_address and so on... as
        ///  App.StudentInfo["name"] ==> returns the student name.
        ///  If a callback is specified then this function also calls the callback function
        ///  Use null if no callback is needed.
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, object> GetStudentInfo(returnDictionaryCallback<string> callback)
        {
            Dictionary<string, string> studentInformation = new Dictionary<string, string>();
            WebClient c = new WebClient();
            c.DownloadStringAsync(new Uri(URITemplate.LAPI_Student_Particulars));
            c.DownloadStringCompleted += new DownloadStringCompletedEventHandler
                (delegate(object sender, DownloadStringCompletedEventArgs e)
                {
                    #region Handler
                    string s = e.Result;
                    using (XmlReader r = XmlReader.Create(new MemoryStream(System.Text.UnicodeEncoding.Unicode.GetBytes(s))))
                    {
                        while (r.Read())
                        {
                            if (r.IsStartElement())
                            {
                                switch (r.Name)
                                {
                                    case "partics":
                                        {
                                            int count = r.AttributeCount;
                                            for (int i = 0; i < count; i++)
                                            {
                                                r.MoveToAttribute(i);
                                                studentInformation[r.Name] = r.Value;
                                            }
                                            r.MoveToElement();
                                            //App.StudentInfo = studentInformation;
                                           
                                            if (callback != null)
                                            {
                                                callback(studentInformation);
                                            }
                                            else
                                            {
                                                studentInfoHandler(studentInformation);
                                            }
                                        } break;
                                }
                            }
                        }
                    }
                    #endregion Handler
                });
            return null;
        }

        private static void studentInfoHandler(Dictionary<string, string> dict)
        {
            //PhoneUtils.ForceAddToDictionary("name",dict["name"], App.StudentInfo);
            PhoneUtils.ForceAddToDictionary("userid", dict["matric"], App.StudentInfo);
            PhoneUtils.ForceAddToDictionary("email", dict["matric"]+"@nus.edu.sg", App.StudentInfo);
            PhoneUtils.ForceAddToDictionary("handphone", dict["home_number"], App.StudentInfo);
        }
    }
}
