﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using NoticeboardApplication.ServiceReference1;
using System.Collections;
namespace NoticeboardApplication
{
    public class IVLEListBoxItem
    {
        public string ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
    public partial class Home : PhoneApplicationPage
    {
        //for notices
        List<Notice> globalBookNotice;
        List<Notice> globalItemNotice;
        List<Notice> globalJobNotice;
        List<Notice> globalCca;
        List<Notice> globalIvle;

        int countBookNotice = 0;
        int countItemNotice = 0;
        int countJobNotice = 0;
        int countCca = 0;
        int countIvle = 0;

        //for requests
        List<Request> globalBookRequest;
        List<Request> globalItemRequest;
        List<Request> globalJobRequest;
        List<Request> globalCca2;
        List<Request> globalIvle2;

        int countBookRequest = 0;
        int countItemRequest = 0;
        int countJobRequest = 0;
        int countIvle2 = 0;



        public Home()
        {
            InitializeComponent();

            globalBookNotice = new List<Notice>();
            globalItemNotice = new List<Notice>();
            globalJobNotice = new List<Notice>();
            globalCca = new List<Notice>();
            globalIvle = new List<Notice>();
            if (!App.IVLEEventCallInProgress)
            {
                LAPICaller.RetrieveFullEvents(cLAPI.API_Token, GetFullEventsCallbackHandler);
                App.IVLEEventCallInProgress = true;
            }
            loadStudentEvents();
            loadPosts();
        }

        private void loadStudentEvents()
        {
            progbar.IsIndeterminate = true;
            if (!App.EventCallInProgress)
            {
                LAPICaller.RetrieveEvents(cLAPI.API_Token, GetEventsCallbackHandler);
                App.EventCallInProgress = true;
            }
        }

        private void GetFullEventsCallbackHandler(Dictionary<string, object> eventDictionary)
        {
            if(App.IVLEEvents.Count>0)
                App.IVLEEvents.Clear();
            foreach (KeyValuePair<string, object> entry in eventDictionary)
            {
                Dictionary<string, string> aEvent = (Dictionary<string, string>)eventDictionary[entry.Key.ToString()];
                App.IVLEEvents.Add(entry.Key.ToString(), aEvent);
                // PhoneUtils.ForceAddToDictionary();
            }
            App.IVLEEventCallInProgress = false;
        }

        private void GetEventsCallbackHandler(Dictionary<string, object> eventDictionary)
        {
            App.EventCallInProgress = false;
            List<IVLEListBoxItem> array = new List<IVLEListBoxItem>();
            foreach (KeyValuePair<string, object> entry in eventDictionary)
            {
                Dictionary<string, string> aEvent = (Dictionary<string, string>)eventDictionary[entry.Key.ToString()];
                array.Add(new IVLEListBoxItem
                {
                    Title = aEvent["Title"].ToString(),
                    Description = aEvent["Organizer"].ToString(),
                    ID = aEvent["ID"].ToString()
                });
                /*foreach (KeyValuePair<string, string> aKeyValuePair in aEvent)
                {
                    //txt_ivle_events.Text += aKeyValuePair.Key.ToString() + "==>" + aKeyValuePair.Value.ToString() + "\n";
                }*/
            }
            IVLEEvents.ItemsSource = array;
            App.EventCallInProgress = false;
            progbar.IsIndeterminate = false;
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            init_book();
            init_item();
            init_job();
            init_cca();
            IVLEEvents.SelectedIndex = -1;
            base.OnNavigatedTo(e);
        }

        private void loadTenNoticesAsync(NoticeboardServiceClient client)
        {
            client.GetNoticesByCategoryTenBookCompleted += new EventHandler<GetNoticesByCategoryTenBookCompletedEventArgs>(client_GetNoticesByCategoryTenBookCompleted);
            client.GetNoticesByCategoryTenBookAsync();

            client.GetNoticesByCategoryTenItemsCompleted += new EventHandler<GetNoticesByCategoryTenItemsCompletedEventArgs>(client_GetNoticesByCategoryTenItemsCompleted);
            client.GetNoticesByCategoryTenItemsAsync();

            client.GetNoticesByCategoryTenJobCompleted += new EventHandler<GetNoticesByCategoryTenJobCompletedEventArgs>(client_GetNoticesByCategoryTenJobCompleted);
            client.GetNoticesByCategoryTenJobAsync();

            client.GetNoticesByCategoryTenCcaCompleted += new EventHandler<GetNoticesByCategoryTenCcaCompletedEventArgs>(client_GetNoticesByCategoryTenCcaCompleted);
            client.GetNoticesByCategoryTenCcaAsync();
        }
        private void loadAllNoticesAsync(NoticeboardServiceClient client)
        {
            client.GetNoticesByCategoryBookCompleted += new EventHandler<GetNoticesByCategoryBookCompletedEventArgs>(client_GetNoticesByCategoryBookCompleted);
            client.GetNoticesByCategoryBookAsync();

            client.GetNoticesByCategoryItemsCompleted += new EventHandler<GetNoticesByCategoryItemsCompletedEventArgs>(client_GetNoticesByCategoryItemsCompleted);
            client.GetNoticesByCategoryItemsAsync();

            client.GetNoticesByCategoryJobCompleted += new EventHandler<GetNoticesByCategoryJobCompletedEventArgs>(client_GetNoticesByCategoryJobCompleted);
            client.GetNoticesByCategoryJobAsync();

            client.GetNoticesByCategoryCcaCompleted += new EventHandler<GetNoticesByCategoryCcaCompletedEventArgs>(client_GetNoticesByCategoryCcaCompleted);
            client.GetNoticesByCategoryCcaAsync();
        }
        private void loadAllRequestAsync(NoticeboardServiceClient client)
        {
            client.GetRequestsByCategoryBookCompleted += new EventHandler<GetRequestsByCategoryBookCompletedEventArgs>(client_GetRequestsByCategoryBookCompleted);
            client.GetRequestsByCategoryBookAsync();

            client.GetRequestsByCategoryItemsCompleted += new EventHandler<GetRequestsByCategoryItemsCompletedEventArgs>(client_GetRequestsByCategoryItemsCompleted);
            client.GetRequestsByCategoryItemsAsync();

            client.GetRequestsByCategoryJobCompleted += new EventHandler<GetRequestsByCategoryJobCompletedEventArgs>(client_GetRequestsByCategoryJobCompleted);
            client.GetRequestsByCategoryJobAsync();
        }
        private void loadPosts()
        {
            NoticeboardServiceClient client = new NoticeboardServiceClient();
            progressbar.IsIndeterminate = true;

            loadTenNoticesAsync(client);
            loadAllNoticesAsync(client);
            loadAllRequestAsync(client);
        }

        #region book
        void init_book()
        {
            BookNotice.SelectedItem = null;
            BookRequest.SelectedItem = null;
            BookRequest.Visibility = System.Windows.Visibility.Collapsed;
            BookNotice.Visibility = System.Windows.Visibility.Visible;
            bookfilter_lp.SelectedIndex = 0;
        }

        void client_GetNoticesByCategoryTenBookCompleted(object sender, GetNoticesByCategoryTenBookCompletedEventArgs e)
        {
            ClearAndAddNoticeToListbox(e.Result.ToList<Notice>(), BookNotice);
            Button book_btn = getShowMoreButton();
            book_btn.Click += new RoutedEventHandler(BookNoticeClick);
            BookNotice.Items.Add(book_btn);
            countBookNotice = 10;
        }

        void client_GetNoticesByCategoryBookCompleted(object sender, GetNoticesByCategoryBookCompletedEventArgs e)
        {
            globalBookNotice = e.Result.ToList<Notice>();
        }

        void client_GetRequestsByCategoryBookCompleted(object sender, GetRequestsByCategoryBookCompletedEventArgs e)
        {
            globalBookRequest = e.Result.ToList<Request>();
            List<Request> temp = globalBookRequest.Skip(countBookRequest).Take(10).ToList<Request>();
            ClearAndAddRequestToListbox(temp, BookRequest);
            Button book_btn = getShowMoreButton();
            book_btn.Click += new RoutedEventHandler(BookRequestClick);
            BookRequest.Items.Add(book_btn);
            countBookRequest = 10;
        }

        void BookNoticeClick(object sender, RoutedEventArgs e)
        {
            List<Notice> temp = globalBookNotice.Skip(countBookNotice).Take(10).ToList<Notice>();
            countBookNotice += 10;
            AppendNoticeToListbox(temp, BookNotice);
        }

        void BookRequestClick(object sender, RoutedEventArgs e)
        {
            List<Request> temp = globalBookRequest.Skip(countBookRequest).Take(10).ToList<Request>();
            countBookRequest += 10;
            AppendRequestToListbox(temp, BookNotice);
        }

        private void bookfilter_lp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //notice vs request switch for book
            if (bookfilter_lp.SelectedIndex == 0)
            {
                BookNotice.Visibility = System.Windows.Visibility.Visible;
                BookRequest.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (bookfilter_lp.SelectedIndex == 1)
            {
                BookRequest.Visibility = System.Windows.Visibility.Visible;
                BookNotice.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void BookNotice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (BookNotice.SelectedItem != null)
            {
                if (BookNotice.SelectedItem is Notice)
                {
                    Notice n = (Notice)BookNotice.SelectedItem;
                    navigate(n);
                }
            }
        }
        private void BookRequest_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (BookRequest.SelectedItem != null)
            {
                if (BookRequest.SelectedItem is Request)
                {
                    Request n = (Request)BookRequest.SelectedItem;
                    navigate(n);
                }
            }
        }
        #endregion book

        #region item
        void init_item()
        {
            ItemNotice.SelectedItem = null;
            ItemRequest.SelectedItem = null;
            ItemNotice.Visibility = System.Windows.Visibility.Visible;
            ItemRequest.Visibility = System.Windows.Visibility.Collapsed;
            itemfilter_lp.SelectedIndex = 0;
        }

        void client_GetNoticesByCategoryTenItemsCompleted(object sender, GetNoticesByCategoryTenItemsCompletedEventArgs e)
        {
            ClearAndAddNoticeToListbox(e.Result.ToList<Notice>(), ItemNotice);
            Button items_btn = getShowMoreButton();
            items_btn.Click += new RoutedEventHandler(ItemNoticeClick);
            ItemNotice.Items.Add(items_btn);
            countItemNotice = 10;
        }
        void client_GetNoticesByCategoryItemsCompleted(object sender, GetNoticesByCategoryItemsCompletedEventArgs e)
        {
            globalItemNotice = e.Result.ToList<Notice>();
        }
        void client_GetRequestsByCategoryItemsCompleted(object sender, GetRequestsByCategoryItemsCompletedEventArgs e)
        {
            globalItemRequest = e.Result.ToList<Request>();
            List<Request> temp = globalItemRequest.Skip(countItemRequest).Take(10).ToList<Request>();
            ClearAndAddRequestToListbox(temp, ItemRequest);
            Button items_btn = getShowMoreButton();
            items_btn.Click += new RoutedEventHandler(ItemRequestClick);
            ItemRequest.Items.Add(items_btn);
            countItemRequest = 10;
        }

        void ItemNoticeClick(object sender, RoutedEventArgs e)
        {
            List<Notice> temp = globalItemNotice.Skip(countItemNotice).Take(10).ToList<Notice>();
            countItemNotice += 10;
            AppendNoticeToListbox(temp, ItemNotice);
        }

        void ItemRequestClick(object sender, RoutedEventArgs e)
        {
            List<Request> temp = globalItemRequest.Skip(countItemRequest).Take(10).ToList<Request>();
            countItemRequest += 10;
            AppendRequestToListbox(temp, ItemRequest);
        }

        private void itemfilter_lp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (itemfilter_lp.SelectedIndex == 0)
            {
                ItemNotice.Visibility = System.Windows.Visibility.Visible;
                ItemRequest.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (itemfilter_lp.SelectedIndex == 1)
            {
                ItemRequest.Visibility = System.Windows.Visibility.Visible;
                ItemNotice.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        private void ItemNotice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ItemNotice.SelectedItem != null)
            {
                if (ItemNotice.SelectedItem is Notice)
                {
                    Notice n = (Notice)ItemNotice.SelectedItem;
                    navigate(n);
                }
            }
        }
        private void ItemRequest_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ItemRequest.SelectedItem != null)
            {
                if (ItemRequest.SelectedItem is Request)
                {
                    Request n = (Request)ItemRequest.SelectedItem;
                    navigate(n);
                }
            }
        }

        #endregion item

        #region job
        private void init_job()
        {
            JobNotice.SelectedItem = null;
            JobRequest.SelectedItem = null;
            JobNotice.Visibility = System.Windows.Visibility.Visible;
            JobRequest.Visibility = System.Windows.Visibility.Collapsed;
            jobfilter_lp.SelectedIndex = 0;
        }
        private void jobfilter_lp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (jobfilter_lp.SelectedIndex == 0)
            {
                JobNotice.Visibility = System.Windows.Visibility.Visible;
                JobRequest.Visibility = System.Windows.Visibility.Collapsed;
            }
            if (jobfilter_lp.SelectedIndex == 1)
            {
                JobRequest.Visibility = System.Windows.Visibility.Visible;
                JobNotice.Visibility = System.Windows.Visibility.Collapsed;
            }
        }

        void client_GetNoticesByCategoryTenJobCompleted(object sender, GetNoticesByCategoryTenJobCompletedEventArgs e)
        {
            ClearAndAddNoticeToListbox(e.Result.ToList<Notice>(), JobNotice);
            Button job_btn = getShowMoreButton();
            job_btn.Click += new RoutedEventHandler(JobNoticeClick);
            JobNotice.Items.Add(job_btn);
            countJobNotice = 10;
        }
        void client_GetNoticesByCategoryJobCompleted(object sender, GetNoticesByCategoryJobCompletedEventArgs e)
        {
            globalJobNotice = e.Result.ToList<Notice>();
        }
        void client_GetRequestsByCategoryJobCompleted(object sender, GetRequestsByCategoryJobCompletedEventArgs e)
        {
            globalJobRequest = e.Result.ToList<Request>();
            List<Request> temp = globalJobRequest.Skip(countJobRequest).Take(10).ToList<Request>();
            ClearAndAddRequestToListbox(temp, JobRequest);
            Button job_btn = getShowMoreButton();
            job_btn.Click += new RoutedEventHandler(JobRequestClick);
            JobRequest.Items.Add(job_btn);
            countJobRequest = 10;
        }
        void JobNoticeClick(object sender, RoutedEventArgs e)
        {
            List<Notice> temp = globalJobNotice.Skip(countJobNotice).Take(10).ToList<Notice>();
            countJobNotice += 10;
            AppendNoticeToListbox(temp, JobNotice);
        }
        void JobRequestClick(object sender, RoutedEventArgs e)
        {
            List<Request> temp = globalJobRequest.Skip(countJobRequest).Take(10).ToList<Request>();
            countJobRequest += 10;
            AppendRequestToListbox(temp, JobNotice);
        }
        private void JobNotice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (JobNotice.SelectedItem != null)
            {
                Notice n = (Notice)JobNotice.SelectedItem;
                navigate(n);
            }
        }
        private void JobRequest_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (JobRequest.SelectedItem != null)
            {
                Request n = (Request)JobRequest.SelectedItem;
                navigate(n);
            }
        }
        #endregion job

        #region cca
        private void init_cca()
        {
            CCAPosts.SelectedItem = null;
        }
        void client_GetNoticesByCategoryTenCcaCompleted(object sender, GetNoticesByCategoryTenCcaCompletedEventArgs e)
        {
            ClearAndAddNoticeToListbox(e.Result.ToList<Notice>(), CCAPosts);
            Button cca_btn = getShowMoreButton();
            cca_btn.Click += new RoutedEventHandler(Cca_Click);
            CCAPosts.Items.Add(cca_btn);
            countCca = 10;
            progressbar.IsIndeterminate = false;
        }
        void client_GetNoticesByCategoryCcaCompleted(object sender, GetNoticesByCategoryCcaCompletedEventArgs e)
        {
            globalCca = e.Result.ToList<Notice>();
        }
        void Cca_Click(object sender, RoutedEventArgs e)
        {
            List<Notice> temp = globalCca.Skip(countCca).Take(10).ToList<Notice>();
            countCca += 10;
            AppendNoticeToListbox(temp, CCAPosts);
        }
        private void CCAPosts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (CCAPosts.SelectedItem != null)
            {
                Notice n = (Notice)CCAPosts.SelectedItem;
                navigate(n);
            }
        }
        #endregion cca

        #region IVLE

        //IVLE Events
        private void ivleEventsSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (IVLEEvents.SelectedItem != null)
            {
                if (IVLEEvents.SelectedItem is IVLEListBoxItem)
                {
                    IVLEListBoxItem n = (IVLEListBoxItem)IVLEEvents.SelectedItem;
                    navigate_IVLE(n);
                }
            }
        }

        private void navigate_IVLE(IVLEListBoxItem n)
        {
            ViewIVLEPost.currentEventID = n.ID;
            NavigationService.Navigate(new Uri("/ViewIVLEPost.xaml", UriKind.Relative));
        }
       
        #endregion IVLE

        #region misc
        private void ClearAndAddNoticeToListbox(List<Notice> list, ListBox listbox)
        {
            listbox.Items.Clear();
            foreach (Notice n in list)
            {
                listbox.Items.Add(n);
            }
        }

        private void ClearAndAddRequestToListbox(List<Request> list, ListBox listbox)
        {
            listbox.Items.Clear();
            foreach (Request n in list)
            {
                listbox.Items.Add(n);
            }
        }

        private void AppendNoticeToListbox(List<Notice> list, ListBox listbox)
        {
            foreach (Notice n in list)
            {
                listbox.Items.Insert(listbox.Items.Count - 1, n);
            }
        }

        private void AppendRequestToListbox(List<Request> list, ListBox listbox)
        {
            foreach (Request n in list)
            {
                listbox.Items.Insert(listbox.Items.Count - 1, n);
            }
        }

        private Button getShowMoreButton()
        {
            Button b = new Button();
            b.Content = "Show More";
            b.Width = 450;
            b.Height = 80;
            return b;
        }

        private void navigate(Notice n)
        {
            App.ViewPost.Clear();
            App.ViewPost.Add("title", n.title);
            App.ViewPost.Add("description", n.description);
            App.ViewPost.Add("price", n.price);
            App.ViewPost.Add("category", n.category);
            App.ViewPost.Add("timestamp", n.timestamp.ToString());
            App.ViewPost.Add("photourl", n.photourl);
            App.ViewPost.Add("userid", n.userid);
            App.ViewPost.Add("type", "Notice");
            App.ViewPost.Add("id", n.id);
            App.ViewPost.Add("handphone", n.phone);
            NavigationService.Navigate(new Uri("/ViewPost.xaml", UriKind.Relative));
        }

        private void navigate(Request n)
        {
            App.ViewPost.Clear();
            App.ViewPost.Add("title", n.title);
            App.ViewPost.Add("description", n.description);
            App.ViewPost.Add("category", n.category);
            App.ViewPost.Add("timestamp", n.timestamp.ToString());
            App.ViewPost.Add("photourl", n.photourl);
            App.ViewPost.Add("userid", n.userid);
            App.ViewPost.Add("type", "Request");
            App.ViewPost.Add("id", n.id);
            App.ViewPost.Add("handphone", n.phone);
            NavigationService.Navigate(new Uri("/ViewPost.xaml", UriKind.Relative));
        }
        private void logo_click(object sender, MouseButtonEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }
        #endregion misc
    }
}