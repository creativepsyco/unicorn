﻿/******
 * THIS Class for declaration of delegates only
 * Do not declare anything else inside it
 * Author : Mohit
 *******/ 

using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace NoticeboardApplication
{
    public delegate void returnBooleanCallback(bool res);
    public delegate void returnEventDictionaryCallback(Dictionary<string, object> EventDictionary);
    public delegate void returnDictionaryCallback<T>(Dictionary<T,T> dict);
    public delegate void returnStringCallback(string str);
    public class EventDelegates
    {

    }
}
