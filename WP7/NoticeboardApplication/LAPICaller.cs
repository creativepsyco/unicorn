﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Xml;
using System.Collections;
using System.IO;
using System.Collections.Generic;

namespace NoticeboardApplication
{
   
    public class LAPICaller
    {
        /// <summary>
        /// Sets up a callback to the function which will decide what to do with the 
        /// request status
        /// </summary>
        /// <param name="token"></param>
        /// <param name="callback">The callback function to be called</param>
        public static void ValidateLogin(string token, returnBooleanCallback callback)
        {
            if (String.IsNullOrEmpty(token)) token = "";
            string requestString = String.Format(URITemplate.LAPI_Validate, cLAPI.APIKey, token);
            //var this_ = this;
            WebClient c = new WebClient();
            c.DownloadStringAsync(new Uri(requestString));
            c.DownloadStringCompleted += new DownloadStringCompletedEventHandler
                (delegate(object sender, DownloadStringCompletedEventArgs e)
            {
                #region Handler
                string s = e.Result;
                using (XmlReader r = XmlReader.Create(new MemoryStream(System.Text.UnicodeEncoding.Unicode.GetBytes(s))))
                {
                    while (r.Read())
                    {
                        if (r.IsStartElement())
                        {
                            switch (r.Name)
                            {
                                case "Success":
                                case "success":
                                    {
                                        if (r.Read())
                                        {
                                            string flag = r.Value;
                                            if (flag == "true" || flag == "True")
                                            {
                                                callback(true);
                                                return;
                                            }
                                            else
                                            {
                                                callback(false);
                                                return;
                                            }

                                        }
                                    } return;
                            }
                        }
                    }
                }
                #endregion Handler
            });
            //callback(false);
        }

        /// <summary>
        /// Retrieves the events Async and parses them into events
        /// then calls the callback
        /// </summary>
        /// <param name="token"></param>
        /// <param name="callback"></param>
        public static void RetrieveFullEvents(string token, returnEventDictionaryCallback callback)
        {
            if (String.IsNullOrEmpty(token)) token = "";
            string requestString = String.Format(URITemplate.LAPI_Student_Events, cLAPI.APIKey, token, "false");
            //var this_ = this;
            WebClient c = new WebClient();
            c.DownloadStringAsync(new Uri(requestString));
            c.DownloadStringCompleted += new DownloadStringCompletedEventHandler
                (delegate(object sender, DownloadStringCompletedEventArgs e)
                {
                    Dictionary<string, object> eventDictionary = new Dictionary<string, object>();
                    string s = e.Result;
                    using (XmlReader r = XmlReader.Create(new MemoryStream(System.Text.UnicodeEncoding.Unicode.GetBytes(s))))
                    {
                        while (r.Read())
                        {
                            ///xxxTODO: Check for validity
                            if (r.IsStartElement() && r.Name == "Results")
                            {
                                while (r.Read())
                                {
                                    if (r.IsStartElement() && r.Name == "Data_StudentEvents")
                                    {
                                        Dictionary<string, string> Eventobj = new Dictionary<string, string>();
                                        while (r.Read())
                                        {
                                            if (r.Name == "Data_StudentEvents" && r.NodeType == XmlNodeType.EndElement) break;
                                            string attr; string data = "";
                                            if (r.IsStartElement() && r.NodeType != XmlNodeType.EndElement)
                                            {
                                                attr = r.Name;
                                                data = "";
                                                if (!r.IsEmptyElement)
                                                {
                                                    r.Read(); data = r.Value.Trim();
                                                }
                                                Eventobj.Add(attr, data);
                                            }
                                        }
                                        eventDictionary.Add(Eventobj["ID"].ToString(), Eventobj);
                                    }
                                }
                            }
                        }
                    }
                    callback(eventDictionary);
                });
        }
        /// <summary>
        /// Retrieves the events Async and parses them into events
        /// then calls the callback
        /// </summary>
        /// <param name="token"></param>
        /// <param name="callback"></param>
        public static void RetrieveEvents(string token, returnEventDictionaryCallback callback)
        {
            if (String.IsNullOrEmpty(token)) token = "";
            string requestString = String.Format(URITemplate.LAPI_Student_Events, cLAPI.APIKey, token,"true");
            //var this_ = this;
            WebClient c = new WebClient();
            c.DownloadStringAsync(new Uri(requestString));
            c.DownloadStringCompleted += new DownloadStringCompletedEventHandler
                (delegate(object sender, DownloadStringCompletedEventArgs e)
            {
                Dictionary<string, object> eventDictionary = new Dictionary<string, object>();
                string s = e.Result;
                using (XmlReader r = XmlReader.Create(new MemoryStream(System.Text.UnicodeEncoding.Unicode.GetBytes(s))))
                {
                    while (r.Read())
                    {
                        ///xxxTODO: Check for validity
                        if (r.IsStartElement() && r.Name == "Results")
                        {
                            while (r.Read())
                            {
                                if (r.IsStartElement() &&  r.Name == "Data_StudentEvents")
                                {
                                    Dictionary<string, string> Eventobj = new Dictionary<string, string>();
                                    while (r.Read())
                                    {
                                        if (r.Name == "Data_StudentEvents" && r.NodeType == XmlNodeType.EndElement) break;
                                        string attr; string data ="";
                                        if(r.IsStartElement() && r.NodeType != XmlNodeType.EndElement)
                                        {
                                            attr = r.Name;
                                            data = "";
                                            if ( !r.IsEmptyElement)
                                            {
                                                r.Read(); data = r.Value.Trim();
                                            }
                                            Eventobj.Add(attr, data);
                                        }
                                    }
                                    eventDictionary.Add(Eventobj["ID"].ToString(), Eventobj);
                                }
                            }
                        }
                    }
                }
                callback(eventDictionary);
            });
        }

        /// <summary>
        /// Retrieves the UserName of the currently logged
        /// in User from IVLE
        /// </summary>
        /// <param name="token"></param>
        /// <param name="callback"></param>
        public static void RetrieveUserName(string token, returnStringCallback callback)
        {
            if (String.IsNullOrEmpty(token)) token = "";
            string requestString = String.Format(URITemplate.LAPI_Student_User_Name, cLAPI.APIKey, token);
            WebClient c = new WebClient();
            c.DownloadStringAsync(new Uri(requestString));
            c.DownloadStringCompleted += new DownloadStringCompletedEventHandler
                (delegate(object sender, DownloadStringCompletedEventArgs e)
                {
                    #region Handler
                    string s = e.Result;
                    using (XmlReader r = XmlReader.Create(new MemoryStream(System.Text.UnicodeEncoding.Unicode.GetBytes(s))))
                    {
                        while (r.Read())
                        {
                            if (r.IsStartElement())
                            {
                                switch (r.Name.ToLower())
                                {
                                    case "string":
                                        {
                                            if (r.Read())
                                            {
                                                string name = r.Value;
                                                callback(name);
                                                return;
                                            }
                                        } break;
                                }
                            }
                        }
                    }
                    #endregion
                });
        }
    }
}
