﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using NoticeboardApplication.ServiceReference1;
using Microsoft.Phone.Tasks;
using System.Windows.Media.Imaging;

namespace NoticeboardApplication
{
    public partial class ViewIVLEPost : PhoneApplicationPage
    {
        public static string currentEventID;
        public ViewIVLEPost()
        {
            InitializeComponent();
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (!string.IsNullOrEmpty(currentEventID) && !App.IVLEEventCallInProgress)
            {
               
                ContentPanel.Visibility = System.Windows.Visibility.Visible;
                progbar.IsIndeterminate = false;
                Dictionary<string, string> aEvent = (Dictionary<string, string>)App.IVLEEvents[currentEventID];
                title_lbl.Text = aEvent["Title"].ToString();
                category_lbl.Text = aEvent["Organizer"].ToString();
                date_lbl.Text = aEvent["EventDateTime"].ToString();
                price_lbl.Text = aEvent["Price"].ToString();
               // description_lbl.Text = aEvent["Description"].ToString();
            }
            else
            {
                progbar.IsIndeterminate = true;
                ContentPanel.Visibility = System.Windows.Visibility.Collapsed;
            }

// <Agenda i:nil="true"/>
//<Contact i:nil="true"/>
//<Description i:nil="true"/>
//<EventDateTime i:nil="true"/>
//<ID>76c1e0a6-9893-4603-bead-337f2176765c</ID>
//<Organizer>NUS Staff Club</Organizer>
//<Price i:nil="true"/>
//<Title>Makeup Essential Workshop (12 Aug 2012)</Title>
//<Venue i:nil="true"/>
  
            base.OnNavigatedTo(e);
        }

        private void webBrowser1_Loaded(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(currentEventID) && !App.IVLEEventCallInProgress)
            {
                Dictionary<string, string> aEvent = (Dictionary<string, string>)App.IVLEEvents[currentEventID];
                webBrowser1.NavigateToString(@aEvent["Description"].ToString());
            }
        }

        private void logo_click(object sender, MouseButtonEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }
    }
}