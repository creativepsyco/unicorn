﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace NoticeboardApplication
{
    public class OfflineStorageClass
    {
        const string USER_TOKEN = "user_token";
        const string USER_FB_TOKEN = "fb_user_token";

        public static string FacebookToken
        {
            get
            {
                if (GetData(USER_FB_TOKEN) != null)
                {
                    return GetData(USER_FB_TOKEN).ToString();
                }
                else
                    return "";
            }
            set
            {
                StoreData(USER_FB_TOKEN, value);
            }
        }

        public static void StoreToken(string token)
        {
            //var settings = System.IO.IsolatedStorage.IsolatedStorageSettings.ApplicationSettings;
            StoreData(USER_TOKEN, token);
            //settings.Save();
        }

        public static string GetToken()
        {
            var settings = System.IO.IsolatedStorage.IsolatedStorageSettings.ApplicationSettings;
            if (settings.Contains(USER_TOKEN))
            {
                if(settings[USER_TOKEN]!=null) return settings[USER_TOKEN].ToString();
            }
            return "";
        }

        public static void StoreData(string key, object value)
        {
            var settings = System.IO.IsolatedStorage.IsolatedStorageSettings.ApplicationSettings;
            if (settings.Contains(key))
            {
                settings[key] = value;
            }
            else
            {
                settings.Add(key, value);
            }
            settings.Save();
        }
        
        /// <summary>
        /// Returns NULL if Key is not in Offline Storage
        /// So Test for NULL
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static object GetData(string key)
        {
            var settings = System.IO.IsolatedStorage.IsolatedStorageSettings.ApplicationSettings;
            if (settings.Contains(key))
            {
                return settings[key];
            }
            else
                return null;
        }
    }
}
