﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace NoticeboardApplication
{
    public partial class Step2 : PhoneApplicationPage
    {
        string defaultDescriptionText = "";
        string defaultpriceText = "";
        public Step2()
        {
            InitializeComponent();
        }
        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (!App.Post.ContainsKey("category"))
            {
                NavigationService.GoBack();
                return;
            }
            switch (App.Post["category"].ToString())
            {
                case "Book":
                    title_lbl.Text = "Book Title:";
                    defaultpriceText = "Amount (SGD). Leave blank if giving away free.";
                    defaultDescriptionText = "Details about the author, publisher, edition, book condition etc.";
                    break;
                case "Item":
                    title_lbl.Text = "Item Name:";
                    defaultpriceText = "Amout (SGD). Leave blank if giving away free.";
                    defaultDescriptionText = "Details about the item, item condition etc.";
                    break;
                case "Job":
                    title_lbl.Text = "Job Title:";
                    defaultpriceText = "Amount that you are paying (SGD):";
                    defaultDescriptionText = "Details about the job, job requirements, skills etc.";
                    break;
                case "Co-Curricular Activity":
                    title_lbl.Text = "Activity Name:";
                    defaultpriceText = "Any fees, activity charges (SGD):";
                    defaultDescriptionText = "Details about the activity, venue, date, time etc.";
                    break;
            }

            if (App.Post["type"].ToString() == "Request")
            {
                price_pnl.Visibility = System.Windows.Visibility.Collapsed;
            }
            sub_description.Text = defaultDescriptionText;
            sub_price.Text = defaultpriceText;
            
            if (App.Post.ContainsKey("title"))
            {
                title_tb.Text = App.Post["title"].ToString();
            }
            if (App.Post.ContainsKey("description"))
            {
                description_tb.Text = App.Post["description"].ToString();
            }
            if (App.Post.ContainsKey("price"))
            {
                price_tb.Text = App.Post["price"].ToString();
            }
            base.OnNavigatedTo(e);
        }
        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (App.Post.ContainsKey("type"))
            {
                App.AddPostArgument("title", title_tb.Text);
                App.AddPostArgument("description", description_tb.Text);
                if (App.Post["type"].ToString() == "Notice")
                {
                    App.AddPostArgument("price", price_tb.Text);
                }
            } 
            
            base.OnNavigatedFrom(e);
        }
        private void PhoneApplicationPage_GotFocus(object sender, RoutedEventArgs e)
        {
            sub_description.Visibility = System.Windows.Visibility.Visible;
            LayoutRoot.Margin = new Thickness(0, -150, 0, 0);
        }

        private void description_tb_LostFocus(object sender, RoutedEventArgs e)
        {
            sub_description.Visibility = System.Windows.Visibility.Collapsed;
            LayoutRoot.Margin = new Thickness(0, 0, 0, 0);
        }

        private void next_btn_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Step2b.xaml", UriKind.Relative));
        }
        private void price_tb_GotFocus(object sender, RoutedEventArgs e)
        {
            sub_price.Text = "Enter the amount in SGD. Leave empty if free.";
            sub_price.Visibility = System.Windows.Visibility.Visible;
            LayoutRoot.Margin = new Thickness(0, -150, 0, 0);
        }

        private void price_tb_LostFocus(object sender, RoutedEventArgs e)
        {
            sub_price.Visibility = System.Windows.Visibility.Collapsed;
            LayoutRoot.Margin = new Thickness(0, 0, 0, 0);
        }
    }
}