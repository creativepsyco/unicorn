﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

namespace NoticeboardApplication
{
    public partial class Step1 : PhoneApplicationPage
    {
        public Step1()
        {
            InitializeComponent();
        }
        protected override void  OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (App.Post.ContainsKey("type"))
            {
                setType(App.Post["type"].ToString());
            }
            if (App.Post.ContainsKey("category"))
            {
                string category = App.Post["category"].ToString();
                if (category == "Cca")
                {
                    category = "Co-Curricular Activity";
                }
                category_lp.SelectedItem = category;
            }

 	        base.OnNavigatedTo(e);
        }
        protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
        {
            string type;
            if (notice_rd.IsChecked == true)
            {
                type = "Notice";
            }
            else
            {
                type = "Request";
            }
            App.AddPostArgument("type", type);
            string category = category_lp.SelectedItem.ToString();
            if(category == "Co-Curricular Activity")
            {
                category = "Cca";
            }
            App.AddPostArgument("category", category);
            base.OnNavigatedFrom(e);
        }

        private void setType(string type)
        {
            if (type == "Notice")
            {
                notice_rd.IsChecked = true;
            }
            else
            {
                request_rd.IsChecked = true;
            }
        }

        private void next_btn_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri("/Step2.xaml", UriKind.Relative));
        }

        private void category_lp_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (category_lp != null)
            {
                panel1.Visibility = System.Windows.Visibility.Visible;
                switch (category_lp.SelectedItem.ToString())
                {
                    case "Book":
                        notice_rd.Content = "Selling";
                        break;
                    case "Item":
                        notice_rd.Content = "Selling";
                        break;
                    case "Job":
                        notice_rd.Content = "Offering";
                        break;
                    case "Co-Curricular Activity":
                        panel1.Visibility = System.Windows.Visibility.Collapsed;
                        break;
                }
            }
        }
    }
}