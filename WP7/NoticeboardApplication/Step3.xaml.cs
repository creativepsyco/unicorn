﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.ServiceModel;
using NoticeboardApplication.ServiceReference1;
using System.Windows.Media.Imaging;
using Microsoft.Phone.Shell;

namespace NoticeboardApplication
{
    public partial class Step3 : PhoneApplicationPage
    {
        bool postStatus = false;
        bool imgUploadStatus = false;
        public Step3()
        {
            InitializeComponent();

         }
        protected override void  OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            if (!App.Post.ContainsKey("title"))
            {
                this.NavigationService.GoBack();
                return;
            }
            title_lbl.Text = App.Post["title"].ToString();
            category_lbl.Text = App.Post["category"].ToString();
            date_lbl.Text = DateTime.Now.ToLongDateString();
            if (App.Post["type"].ToString() == "Notice")
            {
                price_lbl.Text = "$"+App.Post["price"].ToString();
            }
            description_lbl.Text = App.Post["description"].ToString();
            if (App.Post.ContainsKey("photourl"))
            {
                if (App.Post["photourl"].ToString() != "")
                {
                    BitmapImage image = new BitmapImage(new Uri(App.Post["photourl"].ToString()));
                    img.Source = image;
                }
            }
            if (App.Post.ContainsKey("photo"))
            {
                img.Source = (BitmapImage)(App.Post["photo"]);
            }
 	        base.OnNavigatedTo(e);
        }

        private void ApplicationBarIconButton_Click(object sender, EventArgs e)
        {

            progressbar.IsIndeterminate = true;

            var client = new NoticeboardServiceClient();
            string datetimeString = string.Format("{0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
            string imgName = string.Format("image-'{0}'.jpg", datetimeString);
            App.AddPostArgument("userid", App.StudentInfo["userid"].ToString());
            
            uploadImage(client, imgName);
            postNotice(client);

        }
        void uploadImage(NoticeboardServiceClient client, string name)
        {
            if (App.Post.ContainsKey("photo") == true)
            {
                App.AddPostArgument("photourl", name);

                byte[] bytes = ImageService.imageToBytes((BitmapImage)(App.Post["photo"]));

                PictureFile pictureFile = new PictureFile();
                pictureFile.PictureName = name;
                pictureFile.PictureStream = bytes;

                client.PicUploadCompleted += new EventHandler<PicUploadCompletedEventArgs>(client_PicUploadCompleted);
                client.PicUploadAsync(pictureFile);
            }
            else
            {
                if (!App.Post.ContainsKey("photourl"))
                {
                    App.AddPostArgument("photourl", "");
                }
                imgUploadStatus = true;
            }
        }
        void postNotice(NoticeboardServiceClient client)
        {
            if (App.Post.ContainsKey("updating"))
            {
                if ((bool)App.Post["updating"] == true)
                {
                    if (App.Post["type"].ToString() == App.Post["updatetype"].ToString())
                    {
                        //updation
                        if (App.Post["type"].ToString() == "Notice")
                        {
                            client.UpdateNoticeCompleted += new EventHandler<UpdateNoticeCompletedEventArgs>(client_UpdateNoticeCompleted);
                            client.UpdateNoticeAsync(
                                (int)App.Post["id"],
                                App.Post["title"].ToString(),
                                App.Post["description"].ToString(),
                                App.Post["category"].ToString(),
                                App.Post["price"].ToString(),
                                App.Post["photourl"].ToString(),
                                App.Post["userid"].ToString(),
                                App.Post["handphone"].ToString());
                        }
                        else
                        {
                            client.UpdateRequestCompleted += new EventHandler<UpdateRequestCompletedEventArgs>(client_UpdateRequestCompleted);
                            client.UpdateRequestAsync((int)App.Post["id"], App.Post["title"].ToString(), App.Post["description"].ToString(), App.Post["category"].ToString(), App.Post["photourl"].ToString(), App.Post["userid"].ToString(), App.Post["handphone"].ToString());
                        }
                    }
                    else
                    {
                        if (App.Post["updatetype"].ToString() == "Notice")
                        {
                            client.DeleteNoticeAsync((int)App.Post["id"]);
                        }
                        if (App.Post["updatetype"].ToString() == "Request")
                        {
                            client.DeleteRequestAsync((int)App.Post["id"]);
                        }
                        if (App.Post["type"].ToString() == "Notice")
                        {
                            client.CreateNoticeCompleted += new EventHandler<CreateNoticeCompletedEventArgs>(client_CreateNoticeCompleted);
                            client.CreateNoticeAsync(App.Post["title"].ToString(), App.Post["description"].ToString(), App.Post["category"].ToString(), App.Post["price"].ToString(), App.Post["photourl"].ToString(), App.Post["userid"].ToString(), App.Post["handphone"].ToString());
                        }
                        else
                        {
                            client.CreateRequestCompleted += new EventHandler<CreateRequestCompletedEventArgs>(client_CreateRequestCompleted);
                            client.CreateRequestAsync(App.Post["title"].ToString(), App.Post["description"].ToString(), App.Post["category"].ToString(), App.Post["photourl"].ToString(), App.Post["userid"].ToString(), App.Post["handphone"].ToString());
                        }
                    }
                    App.Post.Remove("updating");
                    App.Post.Remove("updatetype");
                    return;
                }
            }
            if (App.Post["type"].ToString() == "Notice")
            {
                client.CreateNoticeCompleted += new EventHandler<CreateNoticeCompletedEventArgs>(client_CreateNoticeCompleted);
                client.CreateNoticeAsync(App.Post["title"].ToString(), App.Post["description"].ToString(), App.Post["category"].ToString(), App.Post["price"].ToString(), App.Post["photourl"].ToString(), App.Post["userid"].ToString(), App.Post["handphone"].ToString());
            }
            else
            {
                client.CreateRequestCompleted += new EventHandler<CreateRequestCompletedEventArgs>(client_CreateRequestCompleted);
                client.CreateRequestAsync(App.Post["title"].ToString(), App.Post["description"].ToString(), App.Post["category"].ToString(), App.Post["photourl"].ToString(), App.Post["userid"].ToString(), App.Post["handphone"].ToString());
            }
  
            
        }

        void client_UpdateRequestCompleted(object sender, UpdateRequestCompletedEventArgs e)
        {
            postStatus = true;
            updateProgressbar();
        }

        void client_UpdateNoticeCompleted(object sender, UpdateNoticeCompletedEventArgs e)
        {
            postStatus = true;
            updateProgressbar();
        }

        void client_PicUploadCompleted(object sender, PicUploadCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    imgUploadStatus = true;
                    updateProgressbar();
                }
                else
                {
                    //ResultTextBlock.Text = "Upload failed :(";
                }
            }
        }
        void client_CreateRequestCompleted(object sender, CreateRequestCompletedEventArgs e)
        {
            postStatus = true;
            updateProgressbar();
        }

        void client_CreateNoticeCompleted(object sender, CreateNoticeCompletedEventArgs e)
        {
            postStatus = true;
            updateProgressbar();
        }

        private void extra_lb_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (extra_lb.SelectedIndex == 0)
            {
                extra_pnl.Visibility = System.Windows.Visibility.Collapsed;
                App.Post.Clear();
                NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
            }
            else if (extra_lb.SelectedIndex == 1)
            {
                if (!App.Post.ContainsKey("title"))
                {
                    MessageBox.Show("Already Posted to facebook");
                    return;
                }
                //A check to see if user is logged in or not
                if (string.IsNullOrEmpty(OfflineStorageClass.FacebookToken))
                {
                    FBAuthAndPost.callPost = true;
                    FBAuthAndPost.callback = booleanCallbackHandler;
                    this.NavigationService.Navigate(new Uri("/FBAuthAndPost.xaml", UriKind.Relative));
                }
                else
                {
                    progressbar.IsIndeterminate = true;
                    FBAuthAndPost.MakeAPost("I am using the NUS Noticeboard to sell/buy/share my stuff.", App.Post["description"].ToString(), "http://google.com", "https://lh3.googleusercontent.com/-pFwcmnUkSP8/TiLlXDMU-pI/AAAAAAAAAyM/89Z2ugd4lZo/nb.png",
                        App.StudentInfo["name"] + " is using Noticeboard", App.Post["title"].ToString(), booleanCallbackHandler);
                    //App.Post.Clear();
                }
            }
            extra_lb.SelectedIndex = -1;
        }
        private void booleanCallbackHandler(bool p)
        {
           
            //Do here to disable fb share 
            if (p)
            {
                App.Post.Clear();
                Dispatcher.BeginInvoke(() => { MessageBox.Show("Successfully Posted to Facebook"); progressbar.IsIndeterminate = false; });
                
            }
            else
            {
                Dispatcher.BeginInvoke(() => { MessageBox.Show("The Posting failed for some reason, Maybe the Access was revoked. Try Again to Authenticate."); progressbar.IsIndeterminate = false; });
                
            }
        }

        void updateProgressbar()
        {
            if (postStatus && imgUploadStatus)
            {
                progressbar.IsIndeterminate = false;
                ExtraPanelAppear.Begin();
                
                ((ApplicationBarIconButton)ApplicationBar.Buttons[0]).IsEnabled = false;
            }
        }
        private void logo_click(object sender, MouseButtonEventArgs e)
        {
            NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
        }
    }
}