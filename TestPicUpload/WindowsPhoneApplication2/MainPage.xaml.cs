﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using System.IO;
using System.Windows.Media.Imaging;

namespace WindowsPhoneApplication2
{
    public partial class MainPage : PhoneApplicationPage
    {
        PhotoChooserTask photoChooserTask;

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Initialize the PhotoChooserTask and assign the Completed handler in the page constructor.
            photoChooserTask = new PhotoChooserTask();
            photoChooserTask.Completed += new EventHandler<PhotoResult>(photoChooserTask_Completed);
        }


        private void button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                photoChooserTask.Show();
            }
            catch (System.InvalidOperationException ex)
            {
                // Catch the exception, but no handling is necessary.
            }
        }



        

        private void uploadBtn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                photoChooserTask.Show();
            }
            catch (System.InvalidOperationException ex)
            {
                // Catch the exception, but no handling is necessary.
            }
        }


        // The Completed event handler. In this example, a new BitmapImage is created and
        // the source is set to the result stream from the PhotoChooserTask
        void photoChooserTask_Completed(object sender, PhotoResult e)
        {
            if (e.TaskResult == TaskResult.OK)
            {
                System.Windows.Media.Imaging.BitmapImage bmp = new System.Windows.Media.Imaging.BitmapImage();

                bmp.SetSource(e.ChosenPhoto);
                byte[] bytes = imageToBytes(bmp);//convert to byte[]


                NoticeboardService.PictureFile pictureFile = new WindowsPhoneApplication2.NoticeboardService.PictureFile();
                pictureFile.PictureName = "pic.jpg"; //NAME OF PIC
                pictureFile.PictureStream = bytes;


                NoticeboardService.NoticeboardServiceClient client = new WindowsPhoneApplication2.NoticeboardService.NoticeboardServiceClient();
                client.PicUploadCompleted += new EventHandler<WindowsPhoneApplication2.NoticeboardService.PicUploadCompletedEventArgs>(client_PicUploadCompleted);
                client.PicUploadAsync(pictureFile);

                /*   
                System.Windows.Media.Imaging.BitmapImage converted = bytesToImage(bytes);
                image2.Source = converted;
                 */
            }
        }


        void client_PicUploadCompleted(object sender, WindowsPhoneApplication2.NoticeboardService.PicUploadCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result)
                {
                    //ResultTextBlock.Text = "Upload succeeded :)";
                }
                else
                {
                    //ResultTextBlock.Text = "Upload failed :(";
                }
            }
        }

        private void downloadBtn_Click(object sender, RoutedEventArgs e)
        {
            NoticeboardService.NoticeboardServiceClient client = new WindowsPhoneApplication2.NoticeboardService.NoticeboardServiceClient();

            client.PicDownloadCompleted += new EventHandler<WindowsPhoneApplication2.NoticeboardService.PicDownloadCompletedEventArgs>(client_PicDownloadCompleted);
            client.PicDownloadAsync("pic"); //NAME OF PIC

        }

        void client_PicDownloadCompleted(object sender, WindowsPhoneApplication2.NoticeboardService.PicDownloadCompletedEventArgs e)
        {
            BitmapImage bitmapImage = new BitmapImage();

            if (e.Error == null)
            {
                if (e.Result != null)
                {
                    NoticeboardService.PictureFile picture = e.Result;
                    MemoryStream stream = new MemoryStream(picture.PictureStream);
                    bitmapImage.SetSource(stream);
                    image2.Source = bitmapImage;
                }
                else
                {
                    //ErrorTextBlock.Text = "No image with that name exists";
                }
            }
        }

        public BitmapImage bytesToImage(byte[] rawImageBytes)
        {
            BitmapImage imageSource = null;

            try
            {
                using (MemoryStream stream = new MemoryStream(rawImageBytes))
                {
                    stream.Seek(0, SeekOrigin.Begin);
                    BitmapImage b = new BitmapImage();
                    b.SetSource(stream);
                    imageSource = b;
                }
            }
            catch (System.Exception ex)
            {
            }

            return imageSource;
        }

        public static byte[] imageToBytes(BitmapImage bitmapImage)
        {
            byte[] data = null;
            using (MemoryStream stream = new MemoryStream())
            {
                WriteableBitmap wBitmap = new WriteableBitmap(bitmapImage);
                wBitmap.SaveJpeg(stream, wBitmap.PixelWidth, wBitmap.PixelHeight, 0, 100);
                stream.Seek(0, SeekOrigin.Begin);
                data = stream.GetBuffer();
            }

            return data;
        }

        

    }
}