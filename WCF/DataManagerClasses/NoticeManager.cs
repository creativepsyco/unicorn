﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;

namespace Noticeboard
{
    public class NoticeManager
    {
        public List<Notice> getAllNotices()
        {
            using (NoticeboardDBDataContext context = new NoticeboardDBDataContext())
            {
                var notices = from n in context.Notices select n;
                return notices.ToList();
            }
        }

        public List<Notice> getAllNoticesTen()
        {
            using (NoticeboardDBDataContext context = new NoticeboardDBDataContext())
            {
                var notices = (from n in context.Notices select n).Take(10);
                return notices.ToList();
            }
        }

        public Notice getNoticeById(int noticeId)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var notice = from n in db.Notices
                             where n.id == noticeId
                              select n;
                return notice.FirstOrDefault();
            }
        }

        public List<Notice> getNoticesByCategory(string category)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var notice = from n in db.Notices
                             where n.category == category
                             select n;
                return notice.ToList();
            }
        }

        public List<Notice> getNoticesByCategoryTen(string category)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var notice = (from n in db.Notices
                             where n.category == category
                             select n).Take(10);
                return notice.ToList();
            }
        }

        public List<Notice> getNoticesByUser(string userId)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var notice = from n in db.Notices
                             where n.userid == userId
                             select n;
                return notice.ToList();
            }
        }

        public List<Notice> getNoticesByUserTen(string userId)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var notice = (from n in db.Notices
                             where n.userid == userId
                             select n).Take(10);
                return notice.ToList();
            }
        }

        public bool createNotice(string title, string description, string category, string price, string photourl, string userId, string phone)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                Notice newNotice = new Notice();
                newNotice.title = title;
                newNotice.description = description;
                newNotice.category = category;
                newNotice.price = price;
                newNotice.photourl = photourl;
                newNotice.userid = userId;
                newNotice.phone = phone;
                newNotice.timestamp = DateTime.Now;
                db.Notices.InsertOnSubmit(newNotice);
                db.SubmitChanges();
                return true;
            }
        }


        public List<Notice> searchNotices(string query)
        {
            string[] str = query.Split(' ');
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var result = db.Notices.Search(str);
                return ((IQueryable<Notice>)result).ToList();
            }
        }

        public List<Notice> searchNoticesTen(string query)
        {
            string[] str = query.Split(' ');
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var result = db.Notices.Search(str);
                List<Notice> list = ((IQueryable<Notice>)result).ToList();

                if (list.Count > 10)
                {
                    List<Notice> rlist = new List<Notice>();
                    for(int i = 0; i < list.Count; i++)
                    {
                        rlist.Add(list[i]);
                    }
                    return rlist;
                }
                else
                {
                    return list;
                }
            }
        }

        public bool deleteNotice(int id)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var notice = from n in db.Notices
                             where n.id == id
                             select n;
                db.Notices.DeleteAllOnSubmit(notice);
                db.SubmitChanges();
                return true;
            }
        }

        public bool updateNotice(int id, string title, string description, string category, string price, string photourl, string userId, string phone)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var res = from n in db.Notices
                             where n.id == id
                             select n;
                Notice notice = res.First();
                notice.title = title;
                notice.description = description;
                notice.category = category;
                notice.price = price;
                notice.photourl = photourl;
                notice.phone = phone;
                notice.timestamp = DateTime.Now;
                notice.userid = userId;
                db.SubmitChanges();
                return true;
            }
        }

        internal List<Notice> getNewRequests()
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var request = (from q in db.Notices
                               orderby q.timestamp descending
                               select q).Take(25);
                return request.ToList();
            }
        }
    }
}