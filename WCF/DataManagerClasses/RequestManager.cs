﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Noticeboard
{
    public class RequestManager
    {
        public List<Request> getAllRequests()
        {
            using (NoticeboardDBDataContext context = new NoticeboardDBDataContext())
            {
                var requests = from q in context.Requests select q;
                return requests.ToList();
            }
        }

        public List<Request> getAllRequestsTen()
        {
            using (NoticeboardDBDataContext context = new NoticeboardDBDataContext())
            {
                var requests = (from q in context.Requests select q).Take(10);
                return requests.ToList();
            }
        }

        public Request getRequestById(int requestId)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var request = from q in db.Requests
                             where q.id == requestId
                             select q;
                return request.FirstOrDefault();
            }
        }

        public List<Request> getRequestsByCategory(string category)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var request = from q in db.Requests
                              where q.category == category
                              select q;
                return request.ToList();
            }
        }

        public List<Request> getRequestsByCategoryTen(string category)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var request = (from q in db.Requests
                              where q.category == category
                              select q).Take(10);
                return request.ToList();
            }
        }

        public List<Request> getRequestsByUser(string userId)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var request = from q in db.Requests
                              where q.userid == userId
                              select q;
                return request.ToList();
            }
        }

        public List<Request> getRequestsByUserTen(string userId)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var request = (from q in db.Requests
                              where q.userid == userId
                              select q).Take(10);
                return request.ToList();
            }
        }

        public bool createRequest(string title, string description, string category, string photourl, string userId, string phone)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                Request newRequest = new Request();
                newRequest.title = title;
                newRequest.description = description;
                newRequest.category = category;
                newRequest.photourl = photourl;
                newRequest.userid = userId;
                newRequest.phone = phone;
                newRequest.timestamp = DateTime.Now;
                db.Requests.InsertOnSubmit(newRequest);
                db.SubmitChanges();
                return true;
            }
        }


        public List<Request> searchRequests(string query)
        {
            string[] str = query.Split(' ');
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var result = db.Requests.Search(str);
                return ((IQueryable<Request>)result).ToList();
            }
        }

        public List<Request> searchRequestsTen(string query)
        {
            string[] str = query.Split(' ');
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var result = db.Requests.Search(str);
                List<Request> list = ((IQueryable<Request>)result).ToList();

                if (list.Count > 10)
                {
                    List<Request> rlist = new List<Request>();
                    for (int i = 0; i < list.Count; i++)
                    {
                        rlist.Add(list[i]);
                    }
                    return rlist;
                }
                else
                {
                    return list;
                }
            }
        }

        public bool deleteRequest(int id)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var request = from q in db.Requests
                             where q.id == id
                             select q;
                db.Requests.DeleteAllOnSubmit(request);
                db.SubmitChanges();
                return true;
            }
        }

        public bool updateRequest(int id, string title, string description, string category, string photourl, string userId, string phone)
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var res = from q in db.Requests
                          where q.id == id
                          select q;
                Request request = res.First();
                request.title = title;
                request.description = description;
                request.category = category;
                request.photourl = photourl;
                request.phone = phone;
                request.timestamp = DateTime.Now;
                request.userid = userId;
                db.SubmitChanges();
                return true;
            }
        }

        internal List<Request> getNewRequests()
        {
            using (NoticeboardDBDataContext db = new NoticeboardDBDataContext())
            {
                var request = (from q in db.Requests
                               orderby q.timestamp descending
                               select q).Take(25);
                return request.ToList();
            }
        }
    }
}