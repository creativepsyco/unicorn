﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.IO;
using System.Configuration;
using System.Web;

namespace Noticeboard
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class NoticeboardService : INoticeboardService
    {
        //Notice mtdz ----------------------------
        public List<Notice> GetAllNotices()
        {
            try
            {
                return (new NoticeManager()).getAllNotices();
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public List<Notice> GetAllNoticesTen() //actually top ten or less
        {
            try
            {
                return (new NoticeManager()).getAllNoticesTen();
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public Notice GetNoticeById(int noticeId)
        {
            try
            {
                return (new NoticeManager()).getNoticeById(noticeId);
            }
            catch (FaultException<Exception> e) { throw e; }
        }



        public List<Notice> GetNoticesByCategory(string category)
        {
            try
            {
                return (new NoticeManager()).getNoticesByCategory(category);
            }
            catch (FaultException<Exception> e) { throw e; }
        }
        public List<Notice> GetNoticesByCategoryBook()
        {
            return GetNoticesByCategory("Book");
        }
        public List<Notice> GetNoticesByCategoryItems()
        {
            return GetNoticesByCategory("Item");
        }
        public List<Notice> GetNoticesByCategoryJob()
        {
            return GetNoticesByCategory("Job");
        }
        public List<Notice> GetNoticesByCategoryCca()
        {
            return GetNoticesByCategory("CCA");
        }
        public List<Notice> GetNoticesByCategoryIvle()
        {
            return GetNoticesByCategory("IVLE");
        }

        public List<Notice> GetNoticesByCategoryTen(string category)
        {
            try
            {
                return (new NoticeManager()).getNoticesByCategoryTen(category);
            }
            catch (FaultException<Exception> e) { throw e; }
        }
        public List<Notice> GetNoticesByCategoryTenBook()
        {
            return GetNoticesByCategoryTen("Book");
        }
        public List<Notice> GetNoticesByCategoryTenItems()
        {
            return GetNoticesByCategoryTen("Item");
        }
        public List<Notice> GetNoticesByCategoryTenJob()
        {
            return GetNoticesByCategoryTen("Job");
        }
        public List<Notice> GetNoticesByCategoryTenCca()
        {
            return GetNoticesByCategoryTen("CCA");
        }
        public List<Notice> GetNoticesByCategoryTenIvle()
        {
            return GetNoticesByCategoryTen("IVLE");
        }

        public List<Notice> GetNoticesByUser(string userId)
        {
            try
            {
                return (new NoticeManager()).getNoticesByUser(userId);
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public List<Notice> GetNoticesByUserTen(string userId)
        {
            try
            {
                return (new NoticeManager()).getNoticesByUserTen(userId);
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public bool CreateNotice(string title, string description, string category, string price, string photourl, string userId, string phone)
        {
            if (photourl == "")
            {
                photourl = "NoImage.png";
            }
            if (photourl.Substring(0, 4) != "http")
            {
                photourl = "http://localhost/Pictures/" + photourl;
            }
            try
            {
                return (new NoticeManager()).createNotice(title, description, category, price, photourl, userId, phone);
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public bool UpdateNotice(int id, string title, string description, string category, string price, string photourl, string userId, string phone)
        {
            if (photourl == "")
            {
                photourl = "NoImage.png";
            }
            if (photourl.Substring(0, 4) != "http")
            {
                photourl = "http://localhost/Pictures/" + photourl;
            }
            try
            {
                return (new NoticeManager()).updateNotice(id, title, description, category, price, photourl, userId, phone);
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public bool DeleteNotice(int id)
        {
            try
            {
                return (new NoticeManager()).deleteNotice(id);
            }
            catch (FaultException<Exception> e) { throw e; }
        }


        public List<Notice> SearchNotices(string query)
        {
            try
            {
                return (new NoticeManager()).searchNotices(query);
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public List<Notice> SearchNoticesTen(string query)
        {
            try
            {
                return (new NoticeManager()).searchNoticesTen(query);
            }
            catch (FaultException<Exception> e) { throw e; }
        }


        //Request mtdz ----------------------------
        public List<Request> GetAllRequests()
        {
            try
            {
                return (new RequestManager()).getAllRequests();
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public List<Request> GetAllRequestsTen()
        {
            try
            {
                return (new RequestManager()).getAllRequestsTen();
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public Request GetRequestById(int requestId)
        {
            try
            {
                return (new RequestManager()).getRequestById(requestId);
            }
            catch (FaultException<Exception> e) { throw e; }
        }


        public List<Request> GetRequestsByCategory(string category)
        {
            try
            {
                return (new RequestManager()).getRequestsByCategory(category);
            }
            catch (FaultException<Exception> e) { throw e; }
        }
        public List<Request> GetRequestsByCategoryBook()
        {
            return GetRequestsByCategory("Book");
        }
        public List<Request> GetRequestsByCategoryItems()
        {
            return GetRequestsByCategory("Item");
        }
        public List<Request> GetRequestsByCategoryJob()
        {
            return GetRequestsByCategory("Job");
        }
        public List<Request> GetRequestsByCategoryCca()
        {
            return GetRequestsByCategory("CCA");
        }
        public List<Request> GetRequestsByCategoryIvle()
        {
            return GetRequestsByCategory("IVLE");
        }

        public List<Request> GetRequestsByCategoryTen(string category)
        {
            try
            {
                return (new RequestManager()).getRequestsByCategoryTen(category);
            }
            catch (FaultException<Exception> e) { throw e; }
        }
        public List<Request> GetRequestsByCategoryTenBook()
        {
            return GetRequestsByCategoryTen("Book");
        }
        public List<Request> GetRequestsByCategoryTenItems()
        {
            return GetRequestsByCategoryTen("Item");
        }
        public List<Request> GetRequestsByCategoryTenJob()
        {
            return GetRequestsByCategoryTen("Job");
        }
        public List<Request> GetRequestsByCategoryTenCca()
        {
            return GetRequestsByCategoryTen("CCA");
        }
        public List<Request> GetRequestsByCategoryTenIvle()
        {
            return GetRequestsByCategoryTen("IVLE");
        }

        public List<Request> GetRequestsByUser(string userId)
        {
            try
            {
                return (new RequestManager()).getRequestsByUser(userId);
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public List<Request> GetRequestsByUserTen(string userId)
        {
            try
            {
                return (new RequestManager()).getRequestsByUserTen(userId);
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public bool CreateRequest(string title, string description, string category, string photourl, string userId, string phone)
        {
            if (photourl == "")
            {
                photourl = "NoImage.png";
            }
            if (photourl.Substring(0, 4) != "http")
            {
                photourl = "http://localhost/Pictures/" + photourl;
            } 
            try
            {
                return (new RequestManager()).createRequest(title, description, category, photourl, userId, phone);
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public bool UpdateRequest(int id, string title, string description, string category, string photourl, string userId, string phone)
        {
            if (photourl == "")
            {
                photourl = "NoImage.png";
            }
            if (photourl.Substring(0, 4) != "http")
            {
                photourl = "http://localhost/Pictures/" + photourl;
            }
            try
            {
                return (new RequestManager()).updateRequest(id, title, description, category, photourl, userId, phone);
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public bool DeleteRequest(int id)
        {
            try
            {
                return (new RequestManager()).deleteRequest(id);
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public List<Request> SearchRequests(string query)
        {
            try
            {
                return (new RequestManager()).searchRequests(query);
            }
            catch (FaultException<Exception> e) { throw e; }
        }

        public List<Request> SearchRequestsTen(string query)
        {
            try
            {
                return (new RequestManager()).searchRequestsTen(query);
            }
            catch (FaultException<Exception> e) { throw e; }
        }


        //Picture Upload Mtd ----------------------------
        public bool PicUpload(PictureFile picture)
        {
            FileStream fileStream = null;
            BinaryWriter writer = null;
            string filePath;

            try
            {
                filePath = "C:" +
                           ConfigurationManager.AppSettings["PictureUploadDirectory"] +
                           picture.PictureName;

                /*This was the line that gave the path O_O
                 * 
                    filePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                           ConfigurationManager.AppSettings["PictureUploadDirectory"] +
                           picture.PictureName;
                 * 
                 * */

                if (picture.PictureName != string.Empty)
                {
                    fileStream = File.Open(filePath, FileMode.Create);
                    writer = new BinaryWriter(fileStream);
                    writer.Write(picture.PictureStream);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                if (fileStream != null)
                    fileStream.Close();
                if (writer != null)
                    writer.Close();
            }
        }

        public PictureFile PicDownload(string pictureName)
        {
            FileStream fileStream = null;
            BinaryReader reader = null;
            string imagePath;
            byte[] imageBytes;

            try
            {
                imagePath = "C:" + ConfigurationManager.AppSettings["PictureUploadDirectory"] + pictureName + ".jpg";
                if (File.Exists(imagePath))
                {
                    fileStream = new FileStream(imagePath, FileMode.Open, FileAccess.Read);
                    reader = new BinaryReader(fileStream);

                    imageBytes = reader.ReadBytes((int)fileStream.Length);

                    return new PictureFile() { PictureName = pictureName, PictureStream = imageBytes };
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }


        public List<Request> GetNewRequests()
        {
            return (new RequestManager()).getNewRequests(); 
        }

        public List<Notice> GetNewNotices()
        {
            return (new NoticeManager()).getNewRequests();
        }
    }
}
