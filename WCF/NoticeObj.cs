﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Runtime.Serialization;

namespace Noticeboard
{
    [DataContract]
    public class NoticeObj:PostObj
    {
        [DataMember]
        public Notice notice { get; set; }

        [DataMember]
        public PictureFile picture { get; set; }
    }
}
