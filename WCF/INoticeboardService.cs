﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
//This is a test for git s
namespace Noticeboard
{

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface INoticeboardService
    {
        //Notice mtdz ----------------------------
        [OperationContract]
        List<Notice> GetAllNotices();

        [OperationContract]
        List<Notice> GetAllNoticesTen();                         //10

        [OperationContract]
        Notice GetNoticeById(int noticeId);

        [OperationContract]
        List<Notice> GetNoticesByCategory(string category);

            [OperationContract]
            List<Notice> GetNoticesByCategoryBook();

            [OperationContract]
            List<Notice> GetNoticesByCategoryItems();

            [OperationContract]
            List<Notice> GetNoticesByCategoryJob();

            [OperationContract]
            List<Notice> GetNoticesByCategoryCca();

            [OperationContract]
            List<Notice> GetNoticesByCategoryIvle();


        [OperationContract]
        List<Notice> GetNoticesByCategoryTen(string category);      //10

            [OperationContract]
            List<Notice> GetNoticesByCategoryTenBook();

            [OperationContract]
            List<Notice> GetNoticesByCategoryTenItems();

            [OperationContract]
            List<Notice> GetNoticesByCategoryTenJob();

            [OperationContract]
            List<Notice> GetNoticesByCategoryTenCca();

            [OperationContract]
            List<Notice> GetNoticesByCategoryTenIvle();



        [OperationContract]
        List<Notice> GetNoticesByUser(string userId);

        [OperationContract]
        List<Notice> GetNoticesByUserTen(string userId);            //10

        [OperationContract]
        bool CreateNotice(string title, string description, string category, string price, string photourl, string userId, string phone);

        [OperationContract]
        bool UpdateNotice(int id, string title, string description, string category, string price, string photourl, string userId, string phone);

        [OperationContract]
        bool DeleteNotice(int id);

        [OperationContract]
        List<Notice> SearchNotices(string query);

        [OperationContract]
        List<Notice> SearchNoticesTen(string query);                 //10



        //Request mtdz ----------------------------
        [OperationContract]
        List<Request> GetAllRequests();

        [OperationContract]
        List<Request> GetAllRequestsTen();                             //10

        [OperationContract]
        Request GetRequestById(int requestId);

        [OperationContract]
        List<Request> GetRequestsByCategory(string category);

            [OperationContract]
            List<Request> GetRequestsByCategoryBook();

            [OperationContract]
            List<Request> GetRequestsByCategoryItems();

            [OperationContract]
            List<Request> GetRequestsByCategoryJob();

            [OperationContract]
            List<Request> GetRequestsByCategoryCca();

            [OperationContract]
            List<Request> GetRequestsByCategoryIvle();


        [OperationContract]
        List<Request> GetRequestsByCategoryTen(string category);       //10

            [OperationContract]
            List<Request> GetRequestsByCategoryTenBook();

            [OperationContract]
            List<Request> GetRequestsByCategoryTenItems();

            [OperationContract]
            List<Request> GetRequestsByCategoryTenJob();

            [OperationContract]
            List<Request> GetRequestsByCategoryTenCca();

            [OperationContract]
            List<Request> GetRequestsByCategoryTenIvle();


        [OperationContract]
        List<Request> GetRequestsByUser(string userId);

        [OperationContract]
        List<Request> GetRequestsByUserTen(string userId);              //10

        [OperationContract]
        bool CreateRequest(string title, string description, string category, string photourl, string userId, string phone);

        [OperationContract]
        bool UpdateRequest(int id, string title, string description, string category, string photourl, string userId, string phone);

        [OperationContract]
        bool DeleteRequest(int id);

        [OperationContract]
        List<Request> SearchRequests(string query);

        [OperationContract]
        List<Request> SearchRequestsTen(string query);                  //10




        //Picture Upload/Download Mtd ----------------------------
        [OperationContract]
        bool PicUpload(PictureFile picture);

        [OperationContract]
        PictureFile PicDownload(string pictureName);

        //Get By Timestamp
        [OperationContract]
        List<Request> GetNewRequests();

        [OperationContract]
        List<Notice> GetNewNotices();
    }
}
